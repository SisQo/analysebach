// Basic column chart
// ------------------------------
var arrayVilles=[];
var arraySecteurs=[];

var Ville =[],totalsVille=[];
const Secteur =[],TotalSecteur=[];

var p1 = $("#fileUploader").change(function(evt){
var selectedFile = evt.target.files[0];
var reader = new FileReader();
reader.onload = function(event) {
var data = event.target.result;
var workbook = XLSX.read(data, {
    type: 'binary'
});
workbook.SheetNames.forEach(function(sheetName) {
  
    var XL_row_object = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName],{raw:true});
    var json_object = JSON.stringify(XL_row_object, null , "\t");
    var x = JSON.parse(json_object);
    // document.getElementById("jsonObject").innerHTML = json_object;

// get villes and push it  to uniqueVille[]
for (let index = 0; index < x.length; index++) {
    arrayVilles.push(x[index].QVille.toLowerCase().trim());
}

var uniqueVille = [...new Set(arrayVilles)];
// 
// get Secteurs and push it  to uniqueSecteur[]
for (let index = 0; index < x.length; index++) {
    arraySecteurs.push(x[index].QSecteur.toLowerCase().trim());
}

var uniqueSecteur = [...new Set(arraySecteurs)];
// 

// Total => Ville
const City_Total = [];

for (let j = 0; j < x.length; j++) {
    for (let i = 0; i < uniqueVille.length; i++) {
        if(uniqueVille[i] == x[j].QVille.toLowerCase().trim()){
            if (!City_Total[uniqueVille[i]]){
                City_Total[uniqueVille[i]] = 1;
            } 
            else City_Total[uniqueVille[i]]++;
        }
    }
}        
//  get ville and total                       
for (let i = 0; i < uniqueVille.length; i++) {
    Ville.push(uniqueVille[i]);
    totalsVille.push(City_Total[uniqueVille[i]]);
}

// END Total Ville
// Total => Secteur
const Secteur_Total = [];

for (let j = 0; j < x.length; j++) {
    for (let i = 0; i < uniqueSecteur.length; i++) {
        if(uniqueSecteur[i] == x[j].QSecteur.toLowerCase().trim()){
            if (!Secteur_Total[uniqueSecteur[i]]){
                Secteur_Total[uniqueSecteur[i]] = 1;
            } 
            else Secteur_Total[uniqueSecteur[i]]++;
        }
    }
}        
//  get ville and total                       
for (let i = 0; i < uniqueSecteur.length; i++) {
    Secteur.push(uniqueSecteur[i]);
    TotalSecteur.push(Secteur_Total[uniqueSecteur[i]]);
}

// END Total Secteur
  })
};

reader.onerror = function(event) {
console.error("File could not be read! Code " + event.target.error.code);
};

reader.readAsBinaryString(selectedFile);

// });
});

function showCharts(){
    //Get the context of the Chart canvas element we want to select
var ctx = $("#column-chart");

// Chart Options
var chartOptions = {
    // Elements options apply to all of the options unless overridden in a dataset
    // In this case, we are setting the border of each bar to be 2px wide and green
    elements: {
        rectangle: {
            borderWidth: 2,
            borderColor: 'rgb(0, 255, 0)',
            borderSkipped: 'bottom'
        }
    },
    responsive: true,
    maintainAspectRatio: false,
    responsiveAnimationDuration:500,
    legend: {
        position: 'top',
    },
    scales: {
        xAxes: [{
            display: true,
            gridLines: {
                color: "#f3f3f3",
                drawTicks: false,
            },
            scaleLabel: {
                display: true,
            }
        }]
    },
    title: {
        display: true,
        text: 'Répartition géographique des PME'
    }
};

// Chart Data
var chartData = {
    // labels: ["January", "February", "March", "April", "May"],
    labels: Ville,
    datasets: [{
        label: "Villes",
        // data: [55, 59, 80, 81, 56],
        data: totalsVille,
        backgroundColor: "#28D094",
        hoverBackgroundColor: "rgba(22,211,154,.9)",
        borderColor: "transparent"
    }]
};

var config = {
    type: 'bar',

    // Chart Options
    options : chartOptions,

    data : chartData
};

// Create the chart
var lineChart = new Chart(ctx, config);
// Graphe secteurs

var ctx1 = $("#SecteursChart");

// Chart Options
var chartOptions = {
    // Elements options apply to all of the options unless overridden in a dataset
    // In this case, we are setting the border of each bar to be 2px wide and green
    elements: {
        rectangle: {
            borderWidth: 2,
            borderColor: 'rgb(0, 255, 0)',
            borderSkipped: 'bottom'
        }
    },
    responsive: true,
    maintainAspectRatio: false,
    responsiveAnimationDuration:500,
    legend: {
        position: 'top',
    },
    scales: {
        xAxes: [{
            display: true,
            gridLines: {
                color: "#f3f3f3",
                drawTicks: false,
            },
            scaleLabel: {
                display: true,
            }
        }]
    },
    title: {
        display: true,
        text: "Répartion des PME par Secteur d'activité"
    }
};

// Chart Data
var chartData = {
    // labels: ["January", "February", "March", "April", "May"],
    labels: Secteur,
    datasets: [{
        label: "Total Réponses",
        // data: [55, 59, 80, 81, 56],
        data: TotalSecteur,
        backgroundColor: "#28D094",
        hoverBackgroundColor: "rgba(22,211,154,.9)",
        borderColor: "transparent"
    }]
};

var config = {
    type: 'bar',

    // Chart Options
    options : chartOptions,

    data : chartData
};

// Create the chart
var lineChart = new Chart(ctx1, config);
// ENd
}