// Basic column chart
// ------------------------------
      var arrayVilles=[];
      var arraySecteurs=[], uniqueSecteur=[];
      var totalsVille=[];
      var arrayNBSalarie=[];
      var arrayNBAge=[];
      var countYes=0,countNon=0,countF=0,countH=0;
      const Ville =[],TotalVille=[];
      const nbrsalarie=[], totalsalarie=[];
      const Age =[],TotalAge=[];
      const Secteur =[],TotalSecteur=[];

      var FVReponse=[],FVTotalReponse=[],ONReponse=[],ONTotalReponse=[];
      var SVReponse=[],SVTotalReponse=[],SVReponse2=[],SVTotalReponse2=[];
      
      var arrayReponse=[];
      

      var arrayQuestions = [' [Améliorer l\'image de l\'entreprise dans la société]',' [Satisfaire les demandes des clients]',' [Faire face à la concurrence]',' [Réduire les coûts]',' [Faire face à la concurrence]'
      ,' [Satisfaire les demandes des clients]',
      ' [Réduire les coûts]',' [Prévenir les accidents et les incidents]',' [Intégrer la SST dans la stratégie d\'entreprise]'
      ,' [Améliorer la satisfaction et la motivation des employés]',' [Eviter les sanctions légales]',
      ' [Améliorer la qualité et la productivité]', ' [Améliorer les conditions de travail]',' [Améliorer le niveau de maîtrise du système de management de la SST]',
      ' [Améliorer la maîtrise et l’organisation de la documentation]',' [Coût de la certification élevé]',' [Ressources humaines et matérielles limitées]',
      ' [Incertitude par rapport aux bénéfices relatifs  à l\'adoption de la norme OHSAS 18001]',
      ' [Non engagement de la direction]',' [Besoin de formation supplémentaire]',
      ' [Non implication des employées]',' [Difficulté de se conformer à la réglementation]',
      ' [Résistance envers le changement]',' [Difficulté dans la méthodologie de déploiement]',
      ' [Non implication de l\'encadrement]',' [Niveau de compétence du responsable SST insuffisant]',
      ' [Objectifs non suffisamment clairs]',' [Faible communication sur les aspects SST]',' [Définition de la politique SST]',' [Définition de la méthode d\'identification des dangers et évaluation des risques]',
      ' [Recherche et application des exigences légales et autres exigences applicables en matière de SST]',
      ' [Etablissement du programme de management en tenant en compte des aspects économiques, des technologies disponibles et des moyens humains]',
      ' [Ressources financières insuffisantes]',' [Formation et sensibilisation de tous les acteurs]',
      ' [Communication interne et externe sur les aspects SST]',
      ' [Rédaction de toutes la documentation relative au système de mangement de la SST]',' [Maîtrise opérationnelle]',
      ' [Traitement des non-conformités et suivi des actions]',' [Réalisation des revues de direction selon une fréquence adaptée]',
      ' [Création d\'un comité de pilotage]',' [Définition du seuil de significativité des aspects]',' [Définition des objectifs et cibles]',
      ' [Temps et ressources humaines non dédiées]',' [Réalisation des audits internes du système de management]'

      ]
      var arrayRaison = [ '[Améliorer l\'image de l\'entreprise dans la société]','[Satisfaire les demandes des clients]','[Faire face à la concurrence]','[Réduire les coûts]','[Faire face à la concurrence]','[Satisfaire les demandes des clients]',
                        '[Réduire les coûts]','[Prévenir les accidents et les incidents]','[Intégrer la SST dans la stratégie d\'entreprise]'
                        ,'[Améliorer la satisfaction et la motivation des employés]','[Eviter les sanctions légales]',
                        '[Améliorer la qualité et la productivité]', '[Améliorer les conditions de travail]','[Améliorer le niveau de maîtrise du système de management de la SST]',
                        '[Améliorer la maîtrise et l’organisation de la documentation]'
                    ];    
    var finalRaisons = [];
    var MoyenDaccord =[], plutotDacord = [],ToutAFDacord = [],plutotPasDacord = [] ,PasDeTDacord = [];

    var arrayObstacle = ['[Coût de la certification élevé]','[Ressources humaines et matérielles limitées]',
                        '[Incertitude par rapport aux bénéfices relatifs  à l\'adoption de la norme OHSAS 18001]',
                        '[Non engagement de la direction]','[Besoin de formation supplémentaire]',
                        '[Non implication des employées]','[Difficulté de se conformer à la réglementation]',
                        '[Résistance envers le changement]','[Difficulté dans la méthodologie de déploiement]',
                        '[Non implication de l\'encadrement]','[Niveau de compétence du responsable SST insuffisant]',
                        '[Objectifs non suffisamment clairs]','[Faible communication sur les aspects SST]'
                        ];
    var finalObstacles = [];
    var OBMoyenDaccord =[], OBplutotDacord = [],OBToutAFDacord = [],OBplutotPasDacord = [] ,OBPasDeTDacord = [];

    var arrayDifficultes = ['[Définition de la politique SST]','[Définition de la méthode d\'identification des dangers et évaluation des risques]',
                            '[Recherche et application des exigences légales et autres exigences applicables en matière de SST]',
                            '[Etablissement du programme de management en tenant en compte des aspects économiques, des technologies disponibles et des moyens humains]',
                            '[Ressources financières insuffisantes]','[Formation et sensibilisation de tous les acteurs]',
                            '[Communication interne et externe sur les aspects SST]',
                            '[Rédaction de toutes la documentation relative au système de mangement de la SST]','[Maîtrise opérationnelle]',
                            '[Traitement des non-conformités et suivi des actions]','[Réalisation des revues de direction selon une fréquence adaptée]',
                            '[Création d\'un comité de pilotage]','[Définition du seuil de significativité des aspects]','[Définition des objectifs et cibles]',
                            '[Temps et ressources humaines non dédiées]','[Réalisation des audits internes du système de management]'
                        ];
    var finalDifficultes = [];
    var DFMoyenDaccord =[], DFplutotDacord = [],DFToutAFDacord = [],DFplutotPasDacord = [] ,DFPasDeTDacord = [];
    var x ;
    var uniqueReponse = [];

    var Raison_TotalBySecteur = [];
    var Raison_TotalByON = [];
    var array1 =[];


$("#fileUploader").change(function(evt){
    var selectedFile = evt.target.files[0];
    var reader = new FileReader();
    reader.onload = function(event) {
      var data = event.target.result;
      var workbook = XLSX.read(data, {
          type: 'binary'
      });
      workbook.SheetNames.forEach(function(sheetName) {
        
          var XL_row_object = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName],{raw:true});
          var json_object = JSON.stringify(XL_row_object, null , "\t");
                x = JSON.parse(json_object);
          // document.getElementById("jsonObject").innerHTML = json_object;

// get villes and push it  to uniqueVille[]
for (let index = 0; index < x.length; index++) {
    
    
    
    arrayVilles.push(x[index].QVille.toLowerCase().trim());
    arraySecteurs.push(x[index].QSecteur.toLowerCase().trim());
    arrayNBSalarie.push(x[index].QNBSalarie);
    arrayNBAge.push(x[index].QAGE);
    if(x[index].QYN == 'Oui'){
        countYes++;
    } 
    if(x[index].QYN == 'Non'){
        countNon++;
    }
    if(x[index].QSex == 'Femme'){
        countF++;
    }
    if(x[index].QSex == 'Homme'){
        countH++;
    }
}



var uniqueVille = [...new Set(arrayVilles)];
    uniqueSecteur = [...new Set(arraySecteurs)];
var uniqueNBSalarie = [...new Set(arrayNBSalarie)];
var uniqueAge = [...new Set(arrayNBAge)];

// Total => Ville
const City_Total = [];

for (let j = 0; j < x.length; j++) {
 for (let i = 0; i < uniqueVille.length; i++) {
  if(uniqueVille[i] == x[j].QVille.toLowerCase().trim()){
    if (!City_Total[uniqueVille[i]]){
      City_Total[uniqueVille[i]] = 1;
    } 
    else City_Total[uniqueVille[i]]++;
  }
 }
}        
//  get ville and total                       
for (let i = 0; i < uniqueVille.length; i++) {
  Ville.push(uniqueVille[i]);
  totalsVille.push(City_Total[uniqueVille[i]]);
}



// END Total Ville
// Total => Secteur
const Secteur_Total = [];

for (let j = 0; j < x.length; j++) {
for (let i = 0; i < uniqueSecteur.length; i++) {
  if(uniqueSecteur[i] == x[j].QSecteur.toLowerCase().trim()){
    if (!Secteur_Total[uniqueSecteur[i]]){
      Secteur_Total[uniqueSecteur[i]] = 1;
    } 
    else Secteur_Total[uniqueSecteur[i]]++;
  }
}
}        
//  get ville and total                       
for (let i = 0; i < uniqueSecteur.length; i++) {
  Secteur.push(uniqueSecteur[i]);
  TotalSecteur.push(Secteur_Total[uniqueSecteur[i]]);
}

// END Total Secteur

// Total => nbrSalarie
const salarie_Total = [];

for (let j = 0; j < x.length; j++) {
 for (let i = 0; i < uniqueNBSalarie.length; i++) {
  if(uniqueNBSalarie[i] == x[j].QNBSalarie){
    if (!salarie_Total[uniqueNBSalarie[i]]){
      salarie_Total[uniqueNBSalarie[i]] = 1;
    } 
    else salarie_Total[uniqueNBSalarie[i]]++;
  }
 }
}        
//  get ville and total                       
for (let i = 0; i < uniqueNBSalarie.length; i++) {
  nbrsalarie.push(uniqueNBSalarie[i]);
  totalsalarie.push(salarie_Total[uniqueNBSalarie[i]]);
}


// END Total nbrSalarie
// Total => AGE
const Age_Total = [];

for (let j = 0; j < x.length; j++) {
 for (let i = 0; i < uniqueAge.length; i++) {
  if(uniqueAge[i] == x[j].QAGE){
    if (!Age_Total[uniqueAge[i]]){
      Age_Total[uniqueAge[i]] = 1;
    } 
    else Age_Total[uniqueAge[i]]++;
  }
 }
}        
//  get ville and total                       
for (let i = 0; i < uniqueAge.length; i++) {
  Age.push(uniqueAge[i]);
  TotalAge.push(Age_Total[uniqueAge[i]]);
}



// END Total AGE

// GET Réponses
uniqueReponse=["Plutôt d’accord","Tout à fait d’accord", "Moyennement d’accord", "Plutôt pas d’accord", "Pas du tout d’accord"]
uniqueCertifs=["ISO 9001" , "ISO14001" , "OHSAS 18001" , "ISO 50001" , "FSSC 2200" , "Label RSE CGEM" , "HACCP" ,
 "QSEEN ET SOCIAL",
"ISOTS16949" , "EN9100" , "Réferentiel Interne Groupe" , "ISO 13006"]

// Function for laste tree
function test(Raisone1){
    Raison_Total = [];
    for (let j = 0; j < x.length; j++) {
        for (let i = 0; i < uniqueReponse.length; i++) {
            if(uniqueReponse[i] == x[j][Raisone1]){
                if (!Raison_Total[uniqueReponse[i]]){
                Raison_Total[uniqueReponse[i]] = 1;
                } 
                else Raison_Total[uniqueReponse[i]]++;
            }
        }
    }
    return Raison_Total;
}

var secteurOptions = "<option selected disabled>Choisir Le Secteur</option>";
var villeOption = "<option selected disabled>Choisir La ville</option>";
var questionOption = "<option selected disabled>Choisir une question</option>";
var certifsOption = "<option selected disabled>Choisir la certification</option>";

// select option secteurs
for (let s = 0; s < uniqueSecteur.length; s++) {
    secteurOptions +="<option>" + uniqueSecteur[s] + "</option>";    
}
// select option Villes
for (let s = 0; s < uniqueVille.length; s++) {
    villeOption +="<option>" + uniqueVille[s] + "</option>";    
}
// select option Questions
for (let s = 0; s < arrayQuestions.length; s++) {
    questionOption +="<option>"+" "+ arrayQuestions[s] + "</option>";    
}
// select option Certificats
for (let s = 0; s < uniqueCertifs.length; s++) {
    certifsOption +="<option>"+" "+ uniqueCertifs[s] + "</option>";    
}



document.getElementById("secteur").innerHTML = secteurOptions;
// document.getElementById("secteur2").innerHTML = secteurOptions;
document.getElementById("ville").innerHTML = villeOption;
// document.getElementById("ville2").innerHTML = villeOption;
document.getElementById("QuestionS").innerHTML = questionOption;
document.getElementById("Question").innerHTML = questionOption;
document.getElementById("Question2").innerHTML = questionOption;
document.getElementById("Question3").innerHTML = questionOption;
document.getElementById("QuestionC").innerHTML = questionOption;
document.getElementById("Certificas").innerHTML = certifsOption;

// end select option secteurs
// Les Raisons

for (let i = 0; i < arrayRaison.length; i++) {
    finalRaisons.push(test(arrayRaison[i]));
    // console.log(arrayRaison[i]);
    // console.log('Raisone : ',i,' ',test(arrayRaison[i]));
}


for (let i = 0; i < finalRaisons.length; i++) {
    MoyenDaccord.push(finalRaisons[i]["Moyennement d’accord"]);
    plutotDacord.push(finalRaisons[i]["Plutôt d’accord"]);
    ToutAFDacord.push(finalRaisons[i]["Tout à fait d’accord"]);
    plutotPasDacord.push(finalRaisons[i]["Plutôt pas d’accord"]);
    PasDeTDacord.push(finalRaisons[i]["Pas du tout d’accord"]);
}


// Les Obstacles
for (let i = 0; i < arrayObstacle.length; i++) {
    finalObstacles.push(test(arrayObstacle[i]));
    // console.log(arrayObstacle[i]);
    // console.log('Obstacle : ',i,' ',test(arrayObstacle[i]));
}

for (let i = 0; i < finalObstacles.length; i++) {
    OBMoyenDaccord.push(finalObstacles[i]["Moyennement d’accord"]);
    OBplutotDacord.push(finalObstacles[i]["Plutôt d’accord"]);
    OBToutAFDacord.push(finalObstacles[i]["Tout à fait d’accord"]);
    OBplutotPasDacord.push(finalObstacles[i]["Plutôt pas d’accord"]);
    OBPasDeTDacord.push(finalObstacles[i]["Pas du tout d’accord"]);
}

// les Difficultés

for (let i = 0; i < arrayDifficultes.length; i++) {
    finalDifficultes.push(test(arrayDifficultes[i]));
    // console.log(arrayDifficultes[i]);
    // console.log('Difficultés : ',i,' ',test(arrayDifficultes[i]));
}

            for (let i = 0; i < finalDifficultes.length; i++) {
                DFMoyenDaccord.push(finalDifficultes[i]["Moyennement d’accord"]);
                DFplutotDacord.push(finalDifficultes[i]["Plutôt d’accord"]);
                DFToutAFDacord.push(finalDifficultes[i]["Tout à fait d’accord"]);
                DFplutotPasDacord.push(finalDifficultes[i]["Plutôt pas d’accord"]);
                DFPasDeTDacord.push(finalDifficultes[i]["Pas du tout d’accord"]);
            }

        })
    };

    reader.onerror = function(event) {
      console.error("File could not be read! Code " + event.target.error.code);
    };

    reader.readAsBinaryString(selectedFile);


// $(window).on("load", function(){

    // Set paths
    // ------------------------------

    require.config({
        paths: {
            echarts: '../../../app-assets/vendors/js/charts/echarts'
        }
    });


    // show button 
    document.getElementById("showRadio").style.display = "block";
//    alert('');
// document.getElementById("alert").innerHTML ='<div class="alert alert-success alert-dismissible">'+
//                                             '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
//                                             ' Operation successful </div>';
   
    
   
// });
});




function testAnalyseBYT(){

    document.getElementById("showSV2").style.display = "block";
    Raisone1=document.getElementById('Question2').value;
    document.getElementById("titleBYT").innerHTML = "<p>Total Réponses Pour la Question : <strong>"+Raisone1+"</strong> pour les Petite,Moyenne et Grande Entreprise ayant : <strong></br> OHSAS 18001 </strong></p>";
    document.getElementById("titleBYTNO").innerHTML = "<p>Total Réponses Pour la Question : <strong>"+Raisone1+"</strong> pour les Petite,Moyenne et Grande Entreprise : <strong></br> n’ayant pas une certification </strong></p>";
    document.getElementById("titleBYTN").innerHTML = "<p>Total Réponses Pour la Question : <strong>"+Raisone1+"</strong> pour les Petite,Moyenne et Grande Entreprise : <strong></br> ayant autre certification que OHSAS 18001 </strong></p>";

    BYTMTotalReponse =[],BYTGTotalReponse =[],BYTPTotalReponse = [],BYTReponse=[];
    Raison_TotalByCErtif = [],Raison_TotalByCErtifN = [];
    Raison_TotalByCErtifG = [],Raison_TotalByCErtifGN = [];
    Raison_TotalByCErtifM = [],Raison_TotalByCErtifMN = [],Raison_TotalByCErtifM1=[];
    Raison_TotalNOCErtif = [],Raison_TotalNOCErtifM = [],Raison_TotalNOCErtifG = [];
    BYTMNOTotalReponse=[],BYTPNOTotalReponse=[],BYTGNOTotalReponse=[];
    BYTMNTotalReponse=[],BYTPNTotalReponse=[],BYTGNTotalReponse=[];

 // Petite Entreprise
 for (let j = 0; j < x.length; j++) {
        
        for (let i = 0; i < uniqueReponse.length; i++) {
   

                for (let k = 0; k < 6; k++) {

                    if(x[j].QCertifications != null && x[j].QCertifications.split(",")[k] == "OHSAS 18001"){
            
                        
                            if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés"){ 
                                if(uniqueReponse[i] == x[j][Raisone1]){

                                        if (!Raison_TotalByCErtif[uniqueReponse[i]]){
                                            Raison_TotalByCErtif[uniqueReponse[i]] = 1;
                                        } 
                                        else Raison_TotalByCErtif[uniqueReponse[i]]++;
                                    }
                            }

                            if(x[j].QNBSalarie == "Entre 100 et 200 salariés"){      
                                if(uniqueReponse[i] == x[j][Raisone1]){

                                        if (!Raison_TotalByCErtifM[uniqueReponse[i]]){
                                            Raison_TotalByCErtifM[uniqueReponse[i]] = 1;
                                        } 
                                        else Raison_TotalByCErtifM[uniqueReponse[i]]++;
                                    }
                            }
                        
                            if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
                                if(uniqueReponse[i] == x[j][Raisone1]){

                                        if (!Raison_TotalByCErtifG[uniqueReponse[i]]){
                                            Raison_TotalByCErtifG[uniqueReponse[i]] = 1;
                                        } 
                                        else Raison_TotalByCErtifG[uniqueReponse[i]]++;
                                    }
                            }

                    }

                    // not OHSAS
                    if(x[j].QCertifications != null){
            
                        
                        if(x[j].QCertifications.split(",")[k] != "OHSAS 18001" && x[j].QCertifications.split(",")[k] != null){
                            
                            if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés"){ 
                            
                                    if(uniqueReponse[i] == x[j][Raisone1]){
                                    
                                    
                                            if (!Raison_TotalByCErtifN[uniqueReponse[i]]){
                                                Raison_TotalByCErtifN[uniqueReponse[i]] = 1;
                                            } 
                                            else Raison_TotalByCErtifN[uniqueReponse[i]]++;
                                    }
                                }

                                if(x[j].QNBSalarie == "Entre 100 et 200 salariés"){  
                            
                                    if(uniqueReponse[i] == x[j][Raisone1]){
                                    
                                    
                                            if (!Raison_TotalByCErtifMN[uniqueReponse[i]]){
                                                Raison_TotalByCErtifMN[uniqueReponse[i]] = 1;
                                            } 
                                            else Raison_TotalByCErtifMN[uniqueReponse[i]]++;
                                    }
                                }

                                if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
                            
                                    if(uniqueReponse[i] == x[j][Raisone1]){
                                    
                                    
                                            if (!Raison_TotalByCErtifGN[uniqueReponse[i]]){
                                                Raison_TotalByCErtifGN[uniqueReponse[i]] = 1;
                                            } 
                                            else Raison_TotalByCErtifGN[uniqueReponse[i]]++;
                                    }
                                }
                        }
                    
                       

                }
                }

                // pas de certification
                if(x[j].QYN == "Non"){
                    if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés"){ 
                        if(uniqueReponse[i] == x[j][Raisone1]){

                                if (!Raison_TotalNOCErtif[uniqueReponse[i]]){
                                    Raison_TotalNOCErtif[uniqueReponse[i]] = 1;
                                } 
                                else Raison_TotalNOCErtif[uniqueReponse[i]]++;
                            }
                    }
                }
                if(x[j].QYN == "Non"){
                    if(x[j].QNBSalarie == "Entre 100 et 200 salariés"){      
                        if(uniqueReponse[i] == x[j][Raisone1]){

                                if (!Raison_TotalNOCErtifM[uniqueReponse[i]]){
                                    Raison_TotalNOCErtifM[uniqueReponse[i]] = 1;
                                } 
                                else Raison_TotalNOCErtifM[uniqueReponse[i]]++;
                            }
                    }
                }
                if(x[j].QYN == "Non"){
                    if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
                        if(uniqueReponse[i] == x[j][Raisone1]){

                                if (!Raison_TotalNOCErtifG[uniqueReponse[i]]){
                                    Raison_TotalNOCErtifG[uniqueReponse[i]] = 1;
                                } 
                                else Raison_TotalNOCErtifG[uniqueReponse[i]]++;
                            }
                    }
                }
    }
}

console.log(Raison_TotalByCErtifM1);

for (let r = 0; r < uniqueReponse.length; r++) {
    BYTReponse.push(uniqueReponse[r]);
    BYTMTotalReponse.push(Raison_TotalByCErtifM[uniqueReponse[r]]);
    BYTPTotalReponse.push(Raison_TotalByCErtif[uniqueReponse[r]]);
    BYTGTotalReponse.push(Raison_TotalByCErtifG[uniqueReponse[r]]);
    // non ohsas
    BYTMNTotalReponse.push(Raison_TotalByCErtifMN[uniqueReponse[r]]);
    BYTPNTotalReponse.push(Raison_TotalByCErtifN[uniqueReponse[r]]);
    BYTGNTotalReponse.push(Raison_TotalByCErtifGN[uniqueReponse[r]]);
    // pas de certification
    BYTMNOTotalReponse.push(Raison_TotalNOCErtifM[uniqueReponse[r]]);
    BYTPNOTotalReponse.push(Raison_TotalNOCErtif[uniqueReponse[r]]);
    BYTGNOTotalReponse.push(Raison_TotalNOCErtifG[uniqueReponse[r]]);
    // console.log('la question : ',Raisone1,'par ville : ',ville,' = ',uniqueReponse[r],' : ', Raison_TotalByVille[uniqueReponse[r]]);  
}

// -----------------------GRAPHE-------------------

document.getElementById("showON").style.display = "block";

document.getElementById("totaloui").innerText = countYes;
document.getElementById("totalnon").innerText = countNon;
document.getElementById("totalF").innerText = countF;
document.getElementById("totalH").innerText = countH;

require(
    [
        'echarts',
        'echarts/chart/bar',
        'echarts/chart/line'
    ],

    function (ec) {

        // Initialize chart
       
        var myChartBYT = ec.init(document.getElementById('TotalBYT'));
        var myChartBYTN = ec.init(document.getElementById('TotalBYTN'));
        var myChartBYTNO = ec.init(document.getElementById('TotalBYTNO'));
    
       
       
        chartOptionsBYT = {

            // Setup grid
            grid: {
                x: 40,
            
                y: 35,
                
            },

            // Add tooltip
            tooltip: {
                trigger: 'axis'
            },

            

            // Add custom colors
            color: ['#66c9c9','#b590ff', '#ffd46f'],

            // Enable drag recalculate
            calculable: false,
            legend: {
                data: ['Petite Entreprises', 'Moyenne Entreprise', 'Grande Entreprise']
            },

            toolbox: {
                show: true,
                orient: 'vertical',
                left: 'right',
                top: 'center',
                feature: {
                    
                    saveAsImage: {show: true}
                }
            },
            // Horizontal axis
            xAxis: [{
                type: 'category',
                
                data : BYTReponse
            }],

            // Vertical axis
            yAxis: [{
                type: 'value'
            }],

            // Add series
            series: [
                // 
                {
                    name: 'Petite Entreprises',
                    type: 'bar',
                    barGap: 0,
                    // label: labelOption,
                    data: BYTPTotalReponse,
                    itemStyle: {
                        normal: {
                            label: {
                                show: true,
                                textStyle: {
                                    fontWeight: 500
                                }
                            }
                        }
                    },
                },
                {
                    name: 'Moyenne Entreprise',
                    type: 'bar',
                    // label: labelOption,
                    data: BYTMTotalReponse,
                    itemStyle: {
                        normal: {
                            label: {
                                show: true,
                                textStyle: {
                                    fontWeight: 500
                                }
                            }
                        }
                    },
                },
                {
                    name: 'Grande Entreprise',
                    type: 'bar',
                    // label: labelOption,
                    data: BYTGTotalReponse,
                    itemStyle: {
                        normal: {
                            label: {
                                show: true,
                                textStyle: {
                                    fontWeight: 500
                                }
                            }
                        }
                    },
                },
               
                
            ]
        };

        // ----------
        chartOptionsBYTNO = {

            // Setup grid
            grid: {
                x: 40,
            
                y: 35,
                
            },

            // Add tooltip
            tooltip: {
                trigger: 'axis'
            },

            

            // Add custom colors
            color: ['#66c9c9','#b590ff', '#ffd46f'],

            // Enable drag recalculate
            calculable: false,
            legend: {
                data: ['Petite Entreprises', 'Moyenne Entreprise', 'Grande Entreprise']
            },

            toolbox: {
                show: true,
                orient: 'vertical',
                left: 'right',
                top: 'center',
                feature: {
                    
                    saveAsImage: {show: true}
                }
            },
            // Horizontal axis
            xAxis: [{
                type: 'category',
                
                data : BYTReponse
            }],

            // Vertical axis
            yAxis: [{
                type: 'value'
            }],

            // Add series
            series: [
                // 
                {
                    name: 'Petite Entreprises',
                    type: 'bar',
                    barGap: 0,
                    // label: labelOption,
                    data: BYTPNOTotalReponse,
                    itemStyle: {
                        normal: {
                            label: {
                                show: true,
                                textStyle: {
                                    fontWeight: 500
                                }
                            }
                        }
                    },
                },
                {
                    name: 'Moyenne Entreprise',
                    type: 'bar',
                    // label: labelOption,
                    data: BYTMNOTotalReponse,
                    itemStyle: {
                        normal: {
                            label: {
                                show: true,
                                textStyle: {
                                    fontWeight: 500
                                }
                            }
                        }
                    },
                },
                {
                    name: 'Grande Entreprise',
                    type: 'bar',
                    // label: labelOption,
                    data: BYTGNOTotalReponse,
                    itemStyle: {
                        normal: {
                            label: {
                                show: true,
                                textStyle: {
                                    fontWeight: 500
                                }
                            }
                        }
                    },
                },
               
                
            ]
        };
// ----------
        chartOptionsBYTN = {

            // Setup grid
            grid: {
                x: 40,
            
                y: 35,
                
            },

            // Add tooltip
            tooltip: {
                trigger: 'axis'
            },

            

            // Add custom colors
            color: ['#66c9c9','#b590ff', '#ffd46f'],

            // Enable drag recalculate
            calculable: false,
            legend: {
                data: ['Petite Entreprises', 'Moyenne Entreprise', 'Grande Entreprise']
            },

            toolbox: {
                show: true,
                orient: 'vertical',
                left: 'right',
                top: 'center',
                feature: {
                    
                    saveAsImage: {show: true}
                }
            },
            // Horizontal axis
            xAxis: [{
                type: 'category',
                
                data : BYTReponse
            }],

            // Vertical axis
            yAxis: [{
                type: 'value'
            }],

            // Add series
            series: [
                // 
                {
                    name: 'Petite Entreprises',
                    type: 'bar',
                    barGap: 0,
                    // label: labelOption,
                    data: BYTPNTotalReponse,
                    itemStyle: {
                        normal: {
                            label: {
                                show: true,
                                textStyle: {
                                    fontWeight: 500
                                }
                            }
                        }
                    },
                },
                {
                    name: 'Moyenne Entreprise',
                    type: 'bar',
                    // label: labelOption,
                    data: BYTMNTotalReponse,
                    itemStyle: {
                        normal: {
                            label: {
                                show: true,
                                textStyle: {
                                    fontWeight: 500
                                }
                            }
                        }
                    },
                },
                {
                    name: 'Grande Entreprise',
                    type: 'bar',
                    // label: labelOption,
                    data: BYTGNTotalReponse,
                    itemStyle: {
                        normal: {
                            label: {
                                show: true,
                                textStyle: {
                                    fontWeight: 500
                                }
                            }
                        }
                    },
                },
               
                
            ]
        };
        myChartBYT.setOption(chartOptionsBYT);
        myChartBYTN.setOption(chartOptionsBYTN);
        myChartBYTNO.setOption(chartOptionsBYTNO);
        // Resize chart
        // ------------------------------

        $(function () {

            // Resize chart on menu width change and window resize
            $(window).on('resize', resize);
            $(".menu-toggle").on('click', resize);

            // Resize function
            function resize() {
                setTimeout(function() {

                    myChartBYT.resize();
                }, 200);
            }
        });
    }
);
}

// ----------------- Niveau 1 ----------------- //
// par ville
function test1_CE(){

    document.getElementById("showCE").style.display = "block";
    Raisone1 = document.getElementById("QuestionC").value;
    certif = document.getElementById("Certificas").value;
    Taill = document.getElementById("Taille4").value;
    
    CEReponse=[];
    CETotalReponse=[];
    Raison_TotalByCErtif = [];
    for (let j = 0; j < x.length; j++) {
        
        for (let i = 0; i < uniqueReponse.length; i++) {

            // Petite Entreprise
            if(Taill == 0){

                for (let k = 0; k < 6; k++) {
           
                    if(x[j].QCertifications != null){
                       
                        if(x[j].QCertifications.split(",")[k] == certif){
                            if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés"){ 
                                if(uniqueReponse[i] == x[j][Raisone1]){

                                        if (!Raison_TotalByCErtif[uniqueReponse[i]]){
                                            Raison_TotalByCErtif[uniqueReponse[i]] = 1;
                                        } 
                                        else Raison_TotalByCErtif[uniqueReponse[i]]++;
                                    }
                            }
                        }
        
                    }
                }
            }
            // Moyen Entreprise
            if(Taill == 1){

                for (let k = 0; k < 6; k++) {
           
                    if(x[j].QCertifications != null){
                       
                        if(x[j].QCertifications.split(",")[k] == certif){
                            if(x[j].QNBSalarie == "Entre 100 et 200 salariés"){      
                                if(uniqueReponse[i] == x[j][Raisone1]){

                                        if (!Raison_TotalByCErtif[uniqueReponse[i]]){
                                            Raison_TotalByCErtif[uniqueReponse[i]] = 1;
                                        } 
                                        else Raison_TotalByCErtif[uniqueReponse[i]]++;
                                    }
                            }
                        }
        
                    }
                }
            }
            // Grande Entreprise
            if(Taill == 2){

                for (let k = 0; k < 6; k++) {
           
                    if(x[j].QCertifications != null){
                       
                        if(x[j].QCertifications.split(",")[k] == certif){
                            if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
                                if(uniqueReponse[i] == x[j][Raisone1]){

                                        if (!Raison_TotalByCErtif[uniqueReponse[i]]){
                                            Raison_TotalByCErtif[uniqueReponse[i]] = 1;
                                        } 
                                        else Raison_TotalByCErtif[uniqueReponse[i]]++;
                                    }
                            }
                        }
        
                    }
                }
            }

        }
            // 
    }
    
    for (let r = 0; r < uniqueReponse.length; r++) {
        CEReponse.push(uniqueReponse[r]);
        CETotalReponse.push(Raison_TotalByCErtif[uniqueReponse[r]]);
        // console.log('la question : ',Raisone1,'par ville : ',ville,' = ',uniqueReponse[r],' : ', Raison_TotalByVille[uniqueReponse[r]]);  
    }

    // alert('Operation successful');
   
    // -----------------------GRAPHE-------------------

    document.getElementById("showCE").style.display = "block";
    document.getElementById("titleVF").innerHTML = "<p>Total Réponses Pour la Question : <strong>"+Raisone1+"</strong> Par rapport a la ville :<strong>"+ville+"</strong> pour les <strong>"+$("#Taille option:selected").text();+"</strong></p>"

    document.getElementById("totaloui").innerText = countYes;
    document.getElementById("totalnon").innerText = countNon;
    document.getElementById("totalF").innerText = countF;
    document.getElementById("totalH").innerText = countH;

    require(
        [
            'echarts',
            'echarts/chart/bar',
            'echarts/chart/line'
        ],

        function (ec) {

            // Initialize chart
           
            var myChartCE = ec.init(document.getElementById('TotalCE'));
        
            // ----------------Filter Question By Ville--------
           
            chartOptionsCE = {

                // Setup grid
                grid: {
                    x: 40,
                
                    y: 35,
                    
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                

                // Add custom colors
                color: ['#ff394f'],

                // Enable drag recalculate
                calculable: false,

                toolbox: {
                    show: true,
                    orient: 'vertical',
                    left: 'right',
                    top: 'center',
                    feature: {
                        
                        saveAsImage: {show: true}
                    }
                },
                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    
                    data : CEReponse
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    // 
                    {
                        name: 'Precipitation',
                        type: 'bar',
                        
                        data: CETotalReponse,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                        
                    }
                ]
            };

            myChartCE.setOption(chartOptionsCE);
            // Resize chart
            // ------------------------------

            $(function () {

                // Resize chart on menu width change and window resize
                $(window).on('resize', resize);
                $(".menu-toggle").on('click', resize);

                // Resize function
                function resize() {
                    setTimeout(function() {

                        myChartCE.resize();
                    }, 200);
                }
            });
        }
    );
}

// par ville
function test1_ville(){

    // document.getElementById("btnShewVF").style.display = "block";
    Raisone1 = document.getElementById("Question").value;
    ville = document.getElementById("ville").value;
    Taill = document.getElementById("Taille").value;
    
    FVReponse=[];
    FVTotalReponse=[];
    Raison_TotalByVille = [];
    for (let j = 0; j < x.length; j++) {
        
        for (let i = 0; i < uniqueReponse.length; i++) {

            // Petite Entreprise
            if(Taill == 0){
            if(x[j].QVille.toLowerCase().trim() == ville){
                
                if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés"){               
                         if(uniqueReponse[i] == x[j][Raisone1]){

                            if (!Raison_TotalByVille[uniqueReponse[i]]){
                                Raison_TotalByVille[uniqueReponse[i]] = 1;
                            } 
                            else Raison_TotalByVille[uniqueReponse[i]]++;
                        }
                    }
                }
            }
            // Moyen Entreprise
            if(Taill == 1){
                if(x[j].QVille.toLowerCase().trim() == ville){
                    
                    if(x[j].QNBSalarie == "Entre 100 et 200 salariés"){               
                             if(uniqueReponse[i] == x[j][Raisone1]){
    
                                if (!Raison_TotalByVille[uniqueReponse[i]]){
                                    Raison_TotalByVille[uniqueReponse[i]] = 1;
                                } 
                                else Raison_TotalByVille[uniqueReponse[i]]++;
                            }
                        }
                    }
                }
                // Grande Entreprise
            if(Taill == 2){
                if(x[j].QVille.toLowerCase().trim() == ville){
                    
                    if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
                             if(uniqueReponse[i] == x[j][Raisone1]){
    
                                if (!Raison_TotalByVille[uniqueReponse[i]]){
                                    Raison_TotalByVille[uniqueReponse[i]] = 1;
                                } 
                                else Raison_TotalByVille[uniqueReponse[i]]++;
                            }
                        }
                    }
                }

            }
            // 
    }
    
    for (let r = 0; r < uniqueReponse.length; r++) {
        FVReponse.push(uniqueReponse[r]);
        FVTotalReponse.push(Raison_TotalByVille[uniqueReponse[r]]);
        // console.log('la question : ',Raisone1,'par ville : ',ville,' = ',uniqueReponse[r],' : ', Raison_TotalByVille[uniqueReponse[r]]);  
    }
    // alert('Operation successful');
   
    // -----------------------GRAPHE-------------------

    document.getElementById("showFV").style.display = "block";
    document.getElementById("titleVF").innerHTML = "<p>Total Réponses Pour la Question : <strong>"+Raisone1+"</strong> Par rapport a la ville :<strong>"+ville+"</strong> pour les <strong>"+$("#Taille option:selected").text();+"</strong></p>"

    document.getElementById("totaloui").innerText = countYes;
    document.getElementById("totalnon").innerText = countNon;
    document.getElementById("totalF").innerText = countF;
    document.getElementById("totalH").innerText = countH;

    require(
        [
            'echarts',
            'echarts/chart/bar',
            'echarts/chart/line'
        ],

        function (ec) {

            // Initialize chart
           
            var myChartFV = ec.init(document.getElementById('TotalFV'));
        
            // ----------------Filter Question By Ville--------
           
            chartOptionsFV = {

                // Setup grid
                grid: {
                    x: 40,
                
                    y: 35,
                    
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                

                // Add custom colors
                color: ['#ff394f'],

                // Enable drag recalculate
                calculable: false,

                toolbox: {
                    show: true,
                    orient: 'vertical',
                    left: 'right',
                    top: 'center',
                    feature: {
                        
                        saveAsImage: {show: true}
                    }
                },
                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    
                    data : FVReponse
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    // 
                    {
                        name: 'Precipitation',
                        type: 'bar',
                        
                        data: FVTotalReponse,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                        
                    }
                ]
            };

            myChartFV.setOption(chartOptionsFV);
            // Resize chart
            // ------------------------------

            $(function () {

                // Resize chart on menu width change and window resize
                $(window).on('resize', resize);
                $(".menu-toggle").on('click', resize);

                // Resize function
                function resize() {
                    setTimeout(function() {

                        myChartFV.resize();
                    }, 200);
                }
            });
        }
    );
}

// Filter par secteur
function test1_Secteur(){
    
    // document.getElementById("btnShewSV").style.display = "block";
    Raisone1 = document.getElementById("QuestionS").value;
    secteur = document.getElementById("secteur").value;
    Taill = document.getElementById("TailleS").value;

    SVReponse=[];
    SVTotalReponse=[];

    Raison_TotalBySecteur = [];
    for (let j = 0; j < x.length; j++) {
        
        for (let i = 0; i < uniqueReponse.length; i++) {

            // // Petite Entreprise
            if(Taill == 0){
            if(x[j].QSecteur.toLowerCase().trim() == secteur){
                if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés"){ 
                    if(uniqueReponse[i] == x[j][Raisone1]){

                        if (!Raison_TotalBySecteur[uniqueReponse[i]]){
                            Raison_TotalBySecteur[uniqueReponse[i]] = 1;
                        } 
                            else Raison_TotalBySecteur[uniqueReponse[i]]++;
                        }
                    }
                }
            }
            // 
             // // Moyen Entreprise
             if(Taill == 1){
             if(x[j].QSecteur.toLowerCase().trim() == secteur){
                if(x[j].QNBSalarie == "Entre 100 et 200 salariés"){               
                    if(uniqueReponse[i] == x[j][Raisone1]){

                        if (!Raison_TotalBySecteur[uniqueReponse[i]]){
                            Raison_TotalBySecteur[uniqueReponse[i]] = 1;
                        } 
                            else Raison_TotalBySecteur[uniqueReponse[i]]++;
                        }
                    }
                }
            }
            // 
             // // Grande Entreprise
             if(Taill == 2){
             if(x[j].QSecteur.toLowerCase().trim() == secteur){
                if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
                    if(uniqueReponse[i] == x[j][Raisone1]){

                        if (!Raison_TotalBySecteur[uniqueReponse[i]]){
                            Raison_TotalBySecteur[uniqueReponse[i]] = 1;
                        } 
                            else Raison_TotalBySecteur[uniqueReponse[i]]++;
                        }
                    }
                }
            }
            // 
        }
            
    }
    
    for (let r = 0; r < uniqueReponse.length; r++) {
        SVReponse.push(uniqueReponse[r]);
        SVTotalReponse.push(Raison_TotalBySecteur[uniqueReponse[r]]);
    }
    // alert('Operation successful');
    // ----------------------------GRAPHE------------------------------------


    document.getElementById("showFS").style.display = "block";
    document.getElementById("titleSF").innerHTML = "<p>Total Réponses Pour la Question : <strong>"+Raisone1+"</strong> Par rapport au secteur :<strong>"+secteur+"</strong></p>"

    document.getElementById("totaloui").innerText = countYes;
    document.getElementById("totalnon").innerText = countNon;
    document.getElementById("totalF").innerText = countF;
    document.getElementById("totalH").innerText = countH;

    require(
        [
            'echarts',
            'echarts/chart/bar',
            'echarts/chart/line'
        ],

        function (ec) {

            // Initialize chart
           
            var myChartSV = ec.init(document.getElementById('TotalSV'));
        
            // ----------------Filter Question By Ville--------
           
            chartOptionsSV = {

                // Setup grid
                grid: {
                    x: 40,
                
                    y: 35,
                    
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                

                // Add custom colors
                color: ['#ff394f'],

                // Enable drag recalculate
                calculable: false,

                toolbox: {
                    show: true,
                    orient: 'vertical',
                    left: 'right',
                    top: 'center',
                    feature: {
                        
                        saveAsImage: {show: true}
                    }
                },
                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data : SVReponse
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    // 
                    {
                        name: 'Precipitation',
                        type: 'bar',
                        
                        data: SVTotalReponse,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                        
                    }
                ]
            };

            myChartSV.setOption(chartOptionsSV);
            // Resize chart
            // ------------------------------

            $(function () {

                // Resize chart on menu width change and window resize
                $(window).on('resize', resize);
                $(".menu-toggle").on('click', resize);

                // Resize function
                function resize() {
                    setTimeout(function() {

                        // Resize chart
                        myChartSV.resize();
                    }, 200);
                }
            });
        }
    );
}
// par OUI NON
function test1_ON(){

    // document.getElementById("btnShewVF").style.display = "block";
    Raisone1 = document.getElementById("Question3").value;
    ON = document.getElementById("ON").value;
    Taill = document.getElementById("Taille3").value;
    
    ONReponse=[];
    ONTotalReponse=[];
    Raison_TotalByON = [];
    for (let j = 0; j < x.length; j++) {
        
        for (let i = 0; i < uniqueReponse.length; i++) {

            // Petite Entreprise
            if(Taill == 0){
            if(x[j].QYN.toLowerCase().trim() == ON){
                
                if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés"){               
                         if(uniqueReponse[i] == x[j][Raisone1]){

                            if (!Raison_TotalByON[uniqueReponse[i]]){
                                Raison_TotalByON[uniqueReponse[i]] = 1;
                            } 
                            else Raison_TotalByON[uniqueReponse[i]]++;
                        }
                    }
                }
            }
            // Moyen Entreprise
            if(Taill == 1){
                if(x[j].QYN.toLowerCase().trim() == ON){
                    
                    if(x[j].QNBSalarie == "Entre 100 et 200 salariés"){               
                             if(uniqueReponse[i] == x[j][Raisone1]){
    
                                if (!Raison_TotalByON[uniqueReponse[i]]){
                                    Raison_TotalByON[uniqueReponse[i]] = 1;
                                } 
                                else Raison_TotalByON[uniqueReponse[i]]++;
                            }
                        }
                    }
                }
                // Grande Entreprise
            if(Taill == 2){
                if(x[j].QYN.toLowerCase().trim() == ON){
                    
                    if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
                             if(uniqueReponse[i] == x[j][Raisone1]){
    
                                if (!Raison_TotalByON[uniqueReponse[i]]){
                                    Raison_TotalByON[uniqueReponse[i]] = 1;
                                } 
                                else Raison_TotalByON[uniqueReponse[i]]++;
                            }
                        }
                    }
                }

            }
            // 
    }
    
    for (let r = 0; r < uniqueReponse.length; r++) {
        ONReponse.push(uniqueReponse[r]);
        ONTotalReponse.push(Raison_TotalByON[uniqueReponse[r]]);
        // console.log('la question : ',Raisone1,'par ville : ',ville,' = ',uniqueReponse[r],' : ', Raison_TotalByVille[uniqueReponse[r]]);  
    }
    // alert('Operation successful');
   
    // -----------------------GRAPHE-------------------

    document.getElementById("showON").style.display = "block";
    document.getElementById("titleON").innerHTML = "<p>Total Réponses Pour la Question : <strong>"+Raisone1+"</strong> pour les <strong>"+$("#Taille3 option:selected").text();+"</strong></p>"

    document.getElementById("totaloui").innerText = countYes;
    document.getElementById("totalnon").innerText = countNon;
    document.getElementById("totalF").innerText = countF;
    document.getElementById("totalH").innerText = countH;

    require(
        [
            'echarts',
            'echarts/chart/bar',
            'echarts/chart/line'
        ],

        function (ec) {

            // Initialize chart
           
            var myChartON = ec.init(document.getElementById('TotalON'));
        
            // ----------------Filter Question By Ville--------
           
            chartOptionsON = {

                // Setup grid
                grid: {
                    x: 40,
                
                    y: 35,
                    
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                

                // Add custom colors
                color: ['#ff394f'],

                // Enable drag recalculate
                calculable: false,

                toolbox: {
                    show: true,
                    orient: 'vertical',
                    left: 'right',
                    top: 'center',
                    feature: {
                        
                        saveAsImage: {show: true}
                    }
                },
                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    
                    data : ONReponse
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    // 
                    {
                        name: 'Precipitation',
                        type: 'bar',
                        
                        data: ONTotalReponse,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                        
                    }
                ]
            };

            myChartON.setOption(chartOptionsON);
            // Resize chart
            // ------------------------------

            $(function () {

                // Resize chart on menu width change and window resize
                $(window).on('resize', resize);
                $(".menu-toggle").on('click', resize);

                // Resize function
                function resize() {
                    setTimeout(function() {

                        myChartON.resize();
                    }, 200);
                }
            });
        }
    );
}
// ----------------- End Niveau 1 ----------- //
// ----------------- Niveau 2 ----------- //
// Filter par secteur
function test2_SV(){
    
    Raisone1 = document.getElementById("Question2").value;
    // secteur = document.getElementById("secteur2").value;
    // ville = document.getElementById("ville2").value;

    SVReponse2=[];
    SVTotalReponse2=[];

    Raison_TotalBySecteur = [];
    for (let j = 0; j < x.length; j++) {
        
        for (let i = 0; i < uniqueReponse.length; i++) {

            // 
            if(x[j].QSecteur.toLowerCase().trim() == secteur){
                if(x[j].QVille.toLowerCase().trim() == ville){

                        if(uniqueReponse[i] == x[j][Raisone1]){

                            if (!Raison_TotalBySecteur[uniqueReponse[i]]){
                                Raison_TotalBySecteur[uniqueReponse[i]] = 1;
                            } 
                            else Raison_TotalBySecteur[uniqueReponse[i]]++;
                        }
                    }
                }
            }
            // 
    }
    
    for (let r = 0; r < uniqueReponse.length; r++) {
        SVReponse2.push(uniqueReponse[r]);
        SVTotalReponse2.push(Raison_TotalBySecteur[uniqueReponse[r]]);
    }
    // alert('Operation successful');
    // ----------------------------------------------------------------

    document.getElementById("showSV2").style.display = "block";
    document.getElementById("titleSV2").innerHTML = "<p>Total Réponses Pour la Question : <strong>"+Raisone1+"</strong> Par rapport au secteur :<strong>"+secteur+"</strong> , Ville : <strong>"+ville+"</strong></p>"

    document.getElementById("totaloui").innerText = countYes;
    document.getElementById("totalnon").innerText = countNon;
    document.getElementById("totalF").innerText = countF;
    document.getElementById("totalH").innerText = countH;

    require(
        [
            'echarts',
            'echarts/chart/bar',
            'echarts/chart/line'
        ],

        function (ec) {

            // Initialize chart
           
            var myChartSV2 = ec.init(document.getElementById('TotalSV2'));
        
            // ----------------Filter Question By Ville--------
           
            chartOptionsSV2 = {

                // Setup grid
                grid: {
                    x: 40,
                
                    y: 35,
                    
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                

                // Add custom colors
                color: ['#ff394f'],

                // Enable drag recalculate
                calculable: false,

                toolbox: {
                    show: true,
                    orient: 'vertical',
                    left: 'right',
                    top: 'center',
                    feature: {
                        
                        saveAsImage: {show: true}
                    }
                },
                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data : SVReponse2
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    // 
                    {
                        name: 'Precipitation',
                        type: 'bar',
                        
                        data: SVTotalReponse2,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                        
                    }
                ]
            };

            myChartSV2.setOption(chartOptionsSV2);
            // Resize chart
            // ------------------------------

            $(function () {

                // Resize chart on menu width change and window resize
                $(window).on('resize', resize);
                $(".menu-toggle").on('click', resize);

                // Resize function
                function resize() {
                    setTimeout(function() {

                        // Resize chart
                        myChartSV2.resize();
                    }, 200);
                }
            });
        }
    );
}

// ----------------- End Niveau 1 ----------- //




function showCharts(){
    document.getElementById("dashboard").style.display = "block";
    document.getElementById("totaloui").innerText = countYes;
    document.getElementById("totalnon").innerText = countNon;
    document.getElementById("totalF").innerText = countF;
    document.getElementById("totalH").innerText = countH;
    require(
        [
            'echarts',
            'echarts/chart/bar',
            'echarts/chart/line'
        ],

        // Start

        

        // Charts setup
        function (ec) {

            // Initialize chart
            // ------------------------------
            var myChartVille = ec.init(document.getElementById('TotalVille'));
            var myChartSecteur = ec.init(document.getElementById('TotalSecteur'));
            var myChartSalarie = ec.init(document.getElementById('TotalSalarie'));
            var myChartAge = ec.init(document.getElementById('TotalAge'));
            var myChartR1 = ec.init(document.getElementById('TotalR1'));
            var myChartOB1 = ec.init(document.getElementById('TotalOB1'));
            var myChartDF1 = ec.init(document.getElementById('TotalDF1'));

            var myChartFV = ec.init(document.getElementById('TotalFV'));
            
            // Chart Options
            // ------------------------------
            chartOptions = {

                // Setup grid
                grid: {
                    x: 40,
                   
                    y: 35,
                    
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                // Add legend
                // legend: {
                //     data: ['Precipitation']
                // },

                // Add custom colors
                color: ['#ff394f'],

                // Enable drag recalculate
                calculable: false,

                toolbox: {
                    show: true,
                    orient: 'vertical',
                    left: 'right',
                    top: 'center',
                    feature: {
                       
                        saveAsImage: {show: true}
                    }
                },
                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    
                    data : Ville
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    // 
                    {
                        name: 'Precipitation',
                        type: 'bar',
                        
                        data: totalsVille,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight:200
                                    }
                                }
                            }
                        },
                        // markLine: {
                        //     data: [{type: 'average', name: 'Average'}]
                        // }
                    }
                ]
            };
            // ------------------------------
            chartOptions1 = {

                // Setup grid
                grid: {
                    x: 40,
                   
                    y: 35,
                    
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

                

                // Add custom colors
                color: ['#ff394f'],

                // Enable drag recalculate
                calculable: false,

                toolbox: {
                    show: true,
                    orient: 'vertical',
                    left: 'right',
                    top: 'center',
                    feature: {
                        
                        saveAsImage: {show: true}
                    }
                },
                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    // data: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
                    data : Secteur
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    // 
                    {
                        name: 'Precipitation',
                        type: 'bar',
                        // data: [2.6, 5.9, 9.0, 26.4, 58.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3],
                        data: TotalSecteur,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                        
                    }
                ]
            };

            // Apply options
            // ------------------------------
            // ------------------------------
            chartOptions2 = {

                // Setup grid
                grid: {
                    x: 40,
                   
                    y: 35,
                    
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

               

                // Add custom colors
                color: ['#ff394f'],

                // Enable drag recalculate
                calculable: false,

                toolbox: {
                    show: true,
                    orient: 'vertical',
                    left: 'right',
                    top: 'center',
                    feature: {
                       
                        saveAsImage: {show: true}
                    }
                },
                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data : nbrsalarie
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    // 
                    {
                       
                        name: 'Precipitation',
                        type: 'bar',
                        data: totalsalarie,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                       
                    }
                ]
            };

            // Apply options
            // ------------------------------
             
             chartOptions3 = {

                // Setup grid
                grid: {
                    x: 40,
                   
                    y: 35,
                    
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis'
                },

               

                // Add custom colors
                color: ['#ff394f'],

                // Enable drag recalculate
                calculable: false,

                toolbox: {
                    show: true,
                    orient: 'vertical',
                    left: 'right',
                    top: 'center',
                    feature: {
                       
                        saveAsImage: {show: true}
                    }
                },
                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    
                    data : Age
                }],

                toolbox: {
                    show: true,
                    orient: 'vertical',
                    left: 'right',
                    top: 'center',
                    feature: {
                        
                        saveAsImage: {show: true}
                    }
                },
                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    // 
                    {
                        name: 'Precipitation',
                        type: 'bar',
                        
                        data: TotalAge,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                        
                    }
                ]
            };

            // ------------------------------
            chartOptionsR1 = {

                // Setup grid
                grid: {
                    x: 40,
                   
                    y: 35,
                    
                },

               
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },

               
                color: ['#FFC300', '#FF5733', '#C70039', '#900C3F','#581845'],

                // Enable drag recalculate
                calculable: false,
                legend: {
                    data: ['Moyennement d’accord', 'Plutôt d’accord', 'Tout à fait d’accord', 'Plutôt pas d’accord','Pas du tout d’accord']
                },
                toolbox: {
                    show: true,
                    orient: 'vertical',
                    left: 'right',
                    top: 'center',
                    feature: {
                        magicType: {show: true, type: ['bar', 'stack']},
                        restore: {show: true},
                        saveAsImage: {show: true}
                    }
                },
                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data : arrayRaison
                    // data: ['Moyennement d’accord', 'Plutôt d’accord','Tout à fait d’accord']
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    // 
                    {
                        name: 'Moyennement d’accord',
                        type: 'bar',
                        barGap: 0,
                        // label: labelOption,
                        data: MoyenDaccord,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                    },
                    {
                        name: 'Plutôt d’accord',
                        type: 'bar',
                        // label: labelOption,
                        data: plutotDacord,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                    },
                    {
                        name: 'Tout à fait d’accord',
                        type: 'bar',
                        // label: labelOption,
                        data: ToutAFDacord,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                    },
                    {
                        name: 'Plutôt pas d’accord',
                        type: 'bar',
                        // label: labelOption,
                        data: plutotPasDacord,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                    },
                    {
                        name: 'Pas du tout d’accord',
                        type: 'bar',
                        // label: labelOption,
                        data: PasDeTDacord,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                    }
                    
                ]
            };

            // Apply options
            // ------------------------------
            chartOptionsOB1 = {

                // Setup grid
                grid: {
                    x: 40,
                   
                    y: 35,
                    
                },

                // Add tooltip
                // tooltip: {
                //     trigger: 'axis'
                // },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },

                // Add legend
                // legend: {
                //     data: ['Precipitation']
                // },

                // Add custom colors
                // color: ['#ff394f'],
                color: ['#FFC300', '#FF5733', '#C70039', '#900C3F','#581845'],

                // Enable drag recalculate
                calculable: false,
                legend: {
                    data: ['Moyennement d’accord', 'Plutôt d’accord', 'Tout à fait d’accord', 'Plutôt pas d’accord','Pas du tout d’accord']
                },
                toolbox: {
                    show: true,
                    orient: 'vertical',
                    left: 'right',
                    top: 'center',
                    feature: {
                        magicType: {show: true, type: ['bar', 'stack']},
                        restore: {show: true},
                        saveAsImage: {show: true}
                    }
                },
                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data : arrayObstacle
                    // data: ['Moyennement d’accord', 'Plutôt d’accord','Tout à fait d’accord']
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    // 
                    {
                        name: 'Moyennement d’accord',
                        type: 'bar',
                        barGap: 0,
                        // label: labelOption,
                        data: OBMoyenDaccord,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                    },
                    {
                        name: 'Plutôt d’accord',
                        type: 'bar',
                        // label: labelOption,
                        data: OBplutotDacord,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                    },
                    {
                        name: 'Tout à fait d’accord',
                        type: 'bar',
                        // label: labelOption,
                        data: OBToutAFDacord,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                    },
                    {
                        name: 'Plutôt pas d’accord',
                        type: 'bar',
                        // label: labelOption,
                        data: OBplutotPasDacord,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                    },
                    {
                        name: 'Pas du tout d’accord',
                        type: 'bar',
                        // label: labelOption,
                        data: OBPasDeTDacord,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                    }
                    
                ]
            };

           
            // ------------------------------
            chartOptionsDF1 = {

                // Setup grid
                grid: {
                    x: 40,
                   
                    y: 35,
                    
                },

                // Add tooltip
                // tooltip: {
                //     trigger: 'axis'
                // },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },

                
                color: ['#FFC300', '#FF5733', '#C70039', '#900C3F','#581845'],

                // Enable drag recalculate
                calculable: false,
                legend: {
                    data: ['Moyennement d’accord', 'Plutôt d’accord', 'Tout à fait d’accord', 'Plutôt pas d’accord','Pas du tout d’accord']
                },
                toolbox: {
                    show: true,
                    orient: 'vertical',
                    left: 'right',
                    top: 'center',
                    feature: {
                        magicType: {show: true, type: ['bar', 'stack']},
                        restore: {show: true},
                        saveAsImage: {show: true}
                    }
                },
                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data : arrayDifficultes
                    // data: ['Moyennement d’accord', 'Plutôt d’accord','Tout à fait d’accord']
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value'
                }],

                // Add series
                series: [
                    // 
                    {
                        name: 'Moyennement d’accord',
                        type: 'bar',
                        barGap: 0,
                        // label: labelOption,
                        data: DFMoyenDaccord,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                    },
                    {
                        name: 'Plutôt d’accord',
                        type: 'bar',
                        // label: labelOption,
                        data: DFplutotDacord,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                    },
                    {
                        name: 'Tout à fait d’accord',
                        type: 'bar',
                        // label: labelOption,
                        data: DFToutAFDacord,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                    },
                    {
                        name: 'Plutôt pas d’accord',
                        type: 'bar',
                        // label: labelOption,
                        data: DFplutotPasDacord,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                    },
                    {
                        name: 'Pas du tout d’accord',
                        type: 'bar',
                        // label: labelOption,
                        data: DFPasDeTDacord,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                    }
                    
                ]
            };

           
            
            

            myChartVille.setOption(chartOptions);
            myChartSecteur.setOption(chartOptions1);
            myChartSalarie.setOption(chartOptions2);
            myChartAge.setOption(chartOptions3);
            myChartR1.setOption(chartOptionsR1);
            myChartOB1.setOption(chartOptionsOB1);
            myChartDF1.setOption(chartOptionsDF1);

            
            // Resize chart
            // ------------------------------

            $(function () {

                // Resize chart on menu width change and window resize
                $(window).on('resize', resize);
                $(".menu-toggle").on('click', resize);

                // Resize function
                function resize() {
                    setTimeout(function() {

                        // Resize chart
                        myChartVille.resize();
                    }, 200);
                }
            });
        }
    );
}
