// Basic column chart
// ------------------------------
var arrayVilles=[];
var arraySecteurs=[], uniqueSecteur=[];
var totalsVille=[];
var arrayNBSalarie=[];
var arrayNBAge=[];
var countYes=0,countNon=0,countF=0,countH=0;
const Ville =[],TotalVille=[];
const nbrsalarie=[], totalsalarie=[];
const Age =[],TotalAge=[];
const Secteur =[],TotalSecteur=[];

var FVReponse=[],FVTotalReponse=[],ONReponse=[],ONTotalReponse=[];
var SVReponse=[],SVTotalReponse=[],SVReponse2=[],SVTotalReponse2=[];

var arrayReponse=[];


var arrayQuestions = [' [Améliorer l\'image de l\'entreprise dans la société]',' [Satisfaire les demandes des clients]',' [Faire face à la concurrence]',' [Réduire les coûts]',' [Faire face à la concurrence]'
,' [Satisfaire les demandes des clients]',
' [Réduire les coûts]',' [Prévenir les accidents et les incidents]',' [Intégrer la SST dans la stratégie d\'entreprise]'
,' [Améliorer la satisfaction et la motivation des employés]',' [Eviter les sanctions légales]',
' [Améliorer la qualité et la productivité]', ' [Améliorer les conditions de travail]',' [Améliorer le niveau de maîtrise du système de management de la SST]',
' [Améliorer la maîtrise et l’organisation de la documentation]',' [Coût de la certification élevé]','[Ressources humaines et matérielles limitées]',
' [Incertitude par rapport aux bénéfices relatifs  à l\'adoption de la norme OHSAS 18001]',
' [Non engagement de la direction]',' [Besoin de formation supplémentaire]',
' [Non implication des employées]',' [Difficulté de se conformer à la réglementation]',
' [Résistance envers le changement]',' [Difficulté dans la méthodologie de déploiement]',
' [Non implication de l\'encadrement]',' [Niveau de compétence du responsable SST insuffisant]',
' [Objectifs non suffisamment clairs]',' [Faible communication sur les aspects SST]',' [Définition de la politique SST]',' [Définition de la méthode d\'identification des dangers et évaluation des risques]',
' [Recherche et application des exigences légales et autres exigences applicables en matière de SST]',
' [Etablissement du programme de management en tenant en compte des aspects économiques, des technologies disponibles et des moyens humains]',
' [Ressources financières insuffisantes]',' [Formation et sensibilisation de tous les acteurs]',
' [Communication interne et externe sur les aspects SST]',
' [Rédaction de toutes la documentation relative au système de mangement de la SST]',' [Maîtrise opérationnelle]',
' [Traitement des non-conformités et suivi des actions]',' [Réalisation des revues de direction selon une fréquence adaptée]',
' [Création d\'un comité de pilotage]',' [Définition du seuil de significativité des aspects]',' [Définition des objectifs et cibles]',
' [Temps et ressources humaines non dédiées]',' [Réalisation des audits internes du système de management]'

]
var arrayRaison = [ '[Améliorer l\'image de l\'entreprise dans la société]','[Satisfaire les demandes des clients]','[Faire face à la concurrence]','[Réduire les coûts]','[Faire face à la concurrence]','[Satisfaire les demandes des clients]',
                  '[Réduire les coûts]','[Prévenir les accidents et les incidents]','[Intégrer la SST dans la stratégie d\'entreprise]'
                  ,'[Améliorer la satisfaction et la motivation des employés]','[Eviter les sanctions légales]',
                  '[Améliorer la qualité et la productivité]', '[Améliorer les conditions de travail]','[Améliorer le niveau de maîtrise du système de management de la SST]',
                  '[Améliorer la maîtrise et l’organisation de la documentation]'
              ];    
var finalRaisons = [];
var MoyenDaccord =[], plutotDacord = [],ToutAFDacord = [],plutotPasDacord = [] ,PasDeTDacord = [];

var arrayObstacle = ['[Coût de la certification élevé]','[Ressources humaines et matérielles limitées]',
                  '[Incertitude par rapport aux bénéfices relatifs  à l\'adoption de la norme OHSAS 18001]',
                  '[Non engagement de la direction]','[Besoin de formation supplémentaire]',
                  '[Non implication des employées]','[Difficulté de se conformer à la réglementation]',
                  '[Résistance envers le changement]','[Difficulté dans la méthodologie de déploiement]',
                  '[Non implication de l\'encadrement]','[Niveau de compétence du responsable SST insuffisant]',
                  '[Objectifs non suffisamment clairs]','[Faible communication sur les aspects SST]'
                  ];
var finalObstacles = [];
var OBMoyenDaccord =[], OBplutotDacord = [],OBToutAFDacord = [],OBplutotPasDacord = [] ,OBPasDeTDacord = [];

var arrayDifficultes = ['[Définition de la politique SST]','[Définition de la méthode d\'identification des dangers et évaluation des risques]',
                      '[Recherche et application des exigences légales et autres exigences applicables en matière de SST]',
                      '[Etablissement du programme de management en tenant en compte des aspects économiques, des technologies disponibles et des moyens humains]',
                      '[Ressources financières insuffisantes]','[Formation et sensibilisation de tous les acteurs]',
                      '[Communication interne et externe sur les aspects SST]',
                      '[Rédaction de toutes la documentation relative au système de mangement de la SST]','[Maîtrise opérationnelle]',
                      '[Traitement des non-conformités et suivi des actions]','[Réalisation des revues de direction selon une fréquence adaptée]',
                      '[Création d\'un comité de pilotage]','[Définition du seuil de significativité des aspects]','[Définition des objectifs et cibles]',
                      '[Temps et ressources humaines non dédiées]','[Réalisation des audits internes du système de management]'
                  ];
var finalDifficultes = [];
var DFMoyenDaccord =[], DFplutotDacord = [],DFToutAFDacord = [],DFplutotPasDacord = [] ,DFPasDeTDacord = [];
var x ;
var uniqueReponse = [];

var Raison_TotalBySecteur = [];
var Raison_TotalByON = [];
var array1 =[];


$("#fileUploader").change(function(evt){
var selectedFile = evt.target.files[0];
var reader = new FileReader();
reader.onload = function(event) {
var data = event.target.result;
var workbook = XLSX.read(data, {
    type: 'binary'
});
workbook.SheetNames.forEach(function(sheetName) {
  
    var XL_row_object = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName],{raw:true});
    var json_object = JSON.stringify(XL_row_object, null , "\t");
          x = JSON.parse(json_object);
    // document.getElementById("jsonObject").innerHTML = json_object;

// get villes and push it  to uniqueVille[]
for (let index = 0; index < x.length; index++) {



arrayVilles.push(x[index].QVille.toLowerCase().trim());
arraySecteurs.push(x[index].QSecteur.toLowerCase().trim());
arrayNBSalarie.push(x[index].QNBSalarie);
arrayNBAge.push(x[index].QAGE);
if(x[index].QYN == 'Oui'){
  countYes++;
} 
if(x[index].QYN == 'Non'){
  countNon++;
}
if(x[index].QSex == 'Femme'){
  countF++;
}
if(x[index].QSex == 'Homme'){
  countH++;
}
}



var uniqueVille = [...new Set(arrayVilles)];
uniqueSecteur = [...new Set(arraySecteurs)];
var uniqueNBSalarie = [...new Set(arrayNBSalarie)];
var uniqueAge = [...new Set(arrayNBAge)];

// Total => Ville
const City_Total = [];

for (let j = 0; j < x.length; j++) {
for (let i = 0; i < uniqueVille.length; i++) {
if(uniqueVille[i] == x[j].QVille.toLowerCase().trim()){
if (!City_Total[uniqueVille[i]]){
City_Total[uniqueVille[i]] = 1;
} 
else City_Total[uniqueVille[i]]++;
}
}
}        
//  get ville and total                       
for (let i = 0; i < uniqueVille.length; i++) {
Ville.push(uniqueVille[i]);
totalsVille.push(City_Total[uniqueVille[i]]);
}



// END Total Ville
// Total => Secteur
const Secteur_Total = [];

for (let j = 0; j < x.length; j++) {
for (let i = 0; i < uniqueSecteur.length; i++) {
if(uniqueSecteur[i] == x[j].QSecteur.toLowerCase().trim()){
if (!Secteur_Total[uniqueSecteur[i]]){
Secteur_Total[uniqueSecteur[i]] = 1;
} 
else Secteur_Total[uniqueSecteur[i]]++;
}
}
}        
//  get ville and total                       
for (let i = 0; i < uniqueSecteur.length; i++) {
Secteur.push(uniqueSecteur[i]);
TotalSecteur.push(Secteur_Total[uniqueSecteur[i]]);
}

// END Total Secteur

// Total => nbrSalarie
const salarie_Total = [];

for (let j = 0; j < x.length; j++) {
for (let i = 0; i < uniqueNBSalarie.length; i++) {
if(uniqueNBSalarie[i] == x[j].QNBSalarie){
if (!salarie_Total[uniqueNBSalarie[i]]){
salarie_Total[uniqueNBSalarie[i]] = 1;
} 
else salarie_Total[uniqueNBSalarie[i]]++;
}
}
}        
//  get ville and total                       
for (let i = 0; i < uniqueNBSalarie.length; i++) {
nbrsalarie.push(uniqueNBSalarie[i]);
totalsalarie.push(salarie_Total[uniqueNBSalarie[i]]);
}


// END Total nbrSalarie
// Total => AGE
const Age_Total = [];

for (let j = 0; j < x.length; j++) {
for (let i = 0; i < uniqueAge.length; i++) {
if(uniqueAge[i] == x[j].QAGE){
if (!Age_Total[uniqueAge[i]]){
Age_Total[uniqueAge[i]] = 1;
} 
else Age_Total[uniqueAge[i]]++;
}
}
}        
//  get ville and total                       
for (let i = 0; i < uniqueAge.length; i++) {
Age.push(uniqueAge[i]);
TotalAge.push(Age_Total[uniqueAge[i]]);
}



// END Total AGE

// GET Réponses
uniqueReponse=["Plutôt d’accord","Tout à fait d’accord", "Moyennement d’accord", "Plutôt pas d’accord", "Pas du tout d’accord"]
uniqueCertifs=["ISO 9001","Réferentiel SAFRAN" , "ISO14001" , "OHSAS 18001" , "ISO 50001" , "FSSC 22000" , "LABEL RSE CGEM" , "HACCP" ,
"QSEEn et social",
"ISOTS16949" , "EN9100" , "Réferentiel Interne Groupe" , "ISO 13006"]

// Function for laste tree
function test(Raisone1){
Raison_Total = [];
for (let j = 0; j < x.length; j++) {
  for (let i = 0; i < uniqueReponse.length; i++) {
      if(uniqueReponse[i] == x[j][Raisone1]){
          if (!Raison_Total[uniqueReponse[i]]){
          Raison_Total[uniqueReponse[i]] = 1;
          } 
          else Raison_Total[uniqueReponse[i]]++;
      }
  }
}
return Raison_Total;
}

var secteurOptions = "<option selected disabled>Choisir Le Secteur</option>";
var secteurOptionsaxe = "<option value='0'>No Secteur</option>";
var certifsOptionaxe = "<option value='0'>No Certification</option>";

var questionOption = "<option selected disabled>Choisir une question</option>";
var certifsOption = "<option selected disabled>Choisir la certification</option>";

// select option secteurs
for (let s = 0; s < uniqueSecteur.length; s++) {
secteurOptions +="<option>" + uniqueSecteur[s] + "</option>";   
secteurOptionsaxe +="<option>" + uniqueSecteur[s] + "</option>"; 
}
// select option Villes

// select option Questions
for (let s = 0; s < arrayQuestions.length; s++) {
questionOption +="<option>"+" "+ arrayQuestions[s] + "</option>";    
}
// select option Certificats
for (let s = 0; s < uniqueCertifs.length; s++) {
certifsOption +="<option>"+" "+ uniqueCertifs[s] + "</option>";    
certifsOptionaxe +="<option>"+" "+ uniqueCertifs[s] + "</option>";    

}

document.getElementById("secteurax123").innerHTML = secteurOptionsaxe;
document.getElementById("CertificasAx123").innerHTML = certifsOptionaxe;
document.getElementById("secteurax2123").innerHTML = secteurOptionsaxe;
document.getElementById("CertificasAx2123").innerHTML = certifsOptionaxe;
document.getElementById("secteurax3123").innerHTML = secteurOptionsaxe;
document.getElementById("CertificasAx3123").innerHTML = certifsOptionaxe;

document.getElementById("secteur").innerHTML = secteurOptions;
document.getElementById("QuestionS").innerHTML = questionOption;
document.getElementById("Question2").innerHTML = questionOption;
document.getElementById("Question3").innerHTML = questionOption;
document.getElementById("QuestionC").innerHTML = questionOption;
document.getElementById("Certificas").innerHTML = certifsOption;

// end select option secteurs
// Les Raisons

for (let i = 0; i < arrayRaison.length; i++) {
finalRaisons.push(test(arrayRaison[i]));
// console.log(arrayRaison[i]);
// console.log('Raisone : ',i,' ',test(arrayRaison[i]));
}


for (let i = 0; i < finalRaisons.length; i++) {
MoyenDaccord.push((finalRaisons[i]["Moyennement d’accord"]/150)*100);
plutotDacord.push((finalRaisons[i]["Plutôt d’accord"]/150)*100);
ToutAFDacord.push((finalRaisons[i]["Tout à fait d’accord"]/150)*100);
plutotPasDacord.push((finalRaisons[i]["Plutôt pas d’accord"]/150)*100);
PasDeTDacord.push((finalRaisons[i]["Pas du tout d’accord"]/150)*100);
}


// Les Obstacles
for (let i = 0; i < arrayObstacle.length; i++) {
finalObstacles.push(test(arrayObstacle[i]));
// console.log(arrayObstacle[i]);
// console.log('Obstacle : ',i,' ',test(arrayObstacle[i]));
}

for (let i = 0; i < finalObstacles.length; i++) {
OBMoyenDaccord.push((finalObstacles[i]["Moyennement d’accord"]/150)*100);
OBplutotDacord.push((finalObstacles[i]["Plutôt d’accord"]/150)*100);
OBToutAFDacord.push((finalObstacles[i]["Tout à fait d’accord"]/150)*100);
OBplutotPasDacord.push((finalObstacles[i]["Plutôt pas d’accord"]/150)*100);
OBPasDeTDacord.push((finalObstacles[i]["Pas du tout d’accord"]/150)*100);
}

// les Difficultés

for (let i = 0; i < arrayDifficultes.length; i++) {
finalDifficultes.push(test(arrayDifficultes[i]));
// console.log(arrayDifficultes[i]);
// console.log('Difficultés : ',i,' ',test(arrayDifficultes[i]));
}


      for (let i = 0; i < finalDifficultes.length; i++) {
      
          DFMoyenDaccord.push((finalDifficultes[i]["Moyennement d’accord"]/150)*100);
          DFplutotDacord.push((finalDifficultes[i]["Plutôt d’accord"]/150)*100);
          DFToutAFDacord.push((finalDifficultes[i]["Tout à fait d’accord"]/150)*100);
          DFplutotPasDacord.push((finalDifficultes[i]["Plutôt pas d’accord"]/150)*100);
          DFPasDeTDacord.push((finalDifficultes[i]["Pas du tout d’accord"]/150)*100);
      }
      
    //   console.log(x);
     
      
  })
};

reader.onerror = function(event) {
console.error("File could not be read! Code " + event.target.error.code);
};

reader.readAsBinaryString(selectedFile);

// ------------------------------

require.config({
  paths: {
      echarts: '../../../app-assets/vendors/js/charts/echarts'
  }
});


// show button 
document.getElementById("showRadio").style.display = "block";
//    alert('');
// document.getElementById("alert").innerHTML ='<div class="alert alert-success alert-dismissible">'+
//                                             '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
//                                             ' Operation successful </div>';



// });

// -----------------------------------------------


    
});

function axes1(){

    Taille = document.getElementById('TailleAxe123').value;
    secteur = document.getElementById('secteurax123').value;
    certif = document.getElementById('CertificasAx123').value;

    var MoyenDaccordAxe1=[],plutotDacordAxe1=[],ToutAFDacordAxe1=[],plutotPasDacordAxe1=[],PasDeTDacordAxe1=[],finalRaisonsAxe1=[];
    if(Taille != 0 && secteur != 0 && certif != 0){
        finalRaisonsAxe11 = [];
        function test1(Raisone1){
            Raison_TotalByCErtif = [];
            
            
            for (let j = 0; j < x.length; j++) {
  
                for (let i = 0; i < uniqueReponse.length; i++) {
              
                    // Petite Entreprise
                    if(Taille == 1){
              
                        for (let k = 0; k < 6; k++) {
                   
                            if(x[j].QCertifications != null){
                               
                                if(x[j].QCertifications.split(",")[k] == certif){
                                    if(x[j].QSecteur.toLowerCase().trim() == secteur){
                                    if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés" || x[j].QNBSalarie == "Entre 100 et 200 salariés"){ 
                                        if(uniqueReponse[i] == x[j][Raisone1]){
              
                                                if (!Raison_TotalByCErtif[uniqueReponse[i]]){
                                                    Raison_TotalByCErtif[uniqueReponse[i]] = 1;
                                                } 
                                                else Raison_TotalByCErtif[uniqueReponse[i]]++;
                                            }
                                    }
                                }
                            }
                
                            }
                        }
                    }
                    
                    // Grande Entreprise
                    if(Taille == 2){
              
                        for (let k = 0; k < 6; k++) {
                   
                            if(x[j].QCertifications != null){
                               
                                if(x[j].QCertifications.split(",")[k] == certif){
                                    if(x[j].QSecteur.toLowerCase().trim() == secteur){
                                    if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
                                        if(uniqueReponse[i] == x[j][Raisone1]){
              
                                                if (!Raison_TotalByCErtif[uniqueReponse[i]]){
                                                    Raison_TotalByCErtif[uniqueReponse[i]] = 1;
                                                } 
                                                else Raison_TotalByCErtif[uniqueReponse[i]]++;
                                            }
                                    }
                                }
                            }
                
                            }
                        }
                    }
              
                }
                    // 
              }
        
            
        
            return Raison_TotalByCErtif;
            }

        // Les Raisons

        for (let i = 0; i < arrayRaison.length; i++) {
            finalRaisonsAxe11.push(test1(arrayRaison[i]));
            }
            
            
            for (let i = 0; i < finalRaisonsAxe11.length; i++) {
                if(finalRaisonsAxe11[i]["Moyennement d’accord"] == null){
                    MoyenDaccordAxe1.push(0);
                }else{
                    MoyenDaccordAxe1.push(finalRaisonsAxe11[i]["Moyennement d’accord"]);
                }
                if(finalRaisonsAxe11[i]["Plutôt d’accord"] == null){
                    plutotDacordAxe1.push(0);
                }else{
                    plutotDacordAxe1.push(finalRaisonsAxe11[i]["Plutôt d’accord"]);
                }
                if(finalRaisonsAxe11[i]["Tout à fait d’accord"] == null){
                    ToutAFDacordAxe1.push(0);
                }else{
                    ToutAFDacordAxe1.push(finalRaisonsAxe11[i]["Tout à fait d’accord"]);
                }
                if(finalRaisonsAxe11[i]["Plutôt pas d’accord"] == null){
                    plutotPasDacordAxe1.push(0);
                }else{
                    plutotPasDacordAxe1.push(finalRaisonsAxe11[i]["Plutôt pas d’accord"]);
                }
                if(finalRaisonsAxe11[i]["Pas du tout d’accord"] == null){
                    PasDeTDacordAxe1.push(0);
                }else{
                    PasDeTDacordAxe1.push(finalRaisonsAxe11[i]["Pas du tout d’accord"]);
                }
                
            }
        console.log("Taille,'-',secteur,'-',certif");
    }else if(certif != 0 && secteur != 0){
        finalRaisonsAxe11 = [];
        function test1(Raisone1){
            Raison_TotalByCErtif = [];
            
            
            for (let j = 0; j < x.length; j++) {
  
                for (let i = 0; i < uniqueReponse.length; i++) {
              
                    // Petite Entreprise
                    
              
                        for (let k = 0; k < 6; k++) {
                   
                            if(x[j].QCertifications != null){
                               
                                if(x[j].QCertifications.split(",")[k] == certif){
                                    if(x[j].QSecteur.toLowerCase().trim() == secteur){
                                        if(uniqueReponse[i] == x[j][Raisone1]){
              
                                                if (!Raison_TotalByCErtif[uniqueReponse[i]]){
                                                    Raison_TotalByCErtif[uniqueReponse[i]] = 1;
                                                } 
                                                else Raison_TotalByCErtif[uniqueReponse[i]]++;
                                            }
                                    }
                                }
                
                            }
                        }
                }
                    // 
              }
        
            
        
            return Raison_TotalByCErtif;
            }

        // Les Raisons

        for (let i = 0; i < arrayRaison.length; i++) {
            finalRaisonsAxe11.push(test1(arrayRaison[i]));
            }
            
            
            for (let i = 0; i < finalRaisonsAxe11.length; i++) {
                if(finalRaisonsAxe11[i]["Moyennement d’accord"] == null){
                    MoyenDaccordAxe1.push(0);
                }else{
                    MoyenDaccordAxe1.push(finalRaisonsAxe11[i]["Moyennement d’accord"]);
                }
                if(finalRaisonsAxe11[i]["Plutôt d’accord"] == null){
                    plutotDacordAxe1.push(0);
                }else{
                    plutotDacordAxe1.push(finalRaisonsAxe11[i]["Plutôt d’accord"]);
                }
                if(finalRaisonsAxe11[i]["Tout à fait d’accord"] == null){
                    ToutAFDacordAxe1.push(0);
                }else{
                    ToutAFDacordAxe1.push(finalRaisonsAxe11[i]["Tout à fait d’accord"]);
                }
                if(finalRaisonsAxe11[i]["Plutôt pas d’accord"] == null){
                    plutotPasDacordAxe1.push(0);
                }else{
                    plutotPasDacordAxe1.push(finalRaisonsAxe11[i]["Plutôt pas d’accord"]);
                }
                if(finalRaisonsAxe11[i]["Pas du tout d’accord"] == null){
                    PasDeTDacordAxe1.push(0);
                }else{
                    PasDeTDacordAxe1.push(finalRaisonsAxe11[i]["Pas du tout d’accord"]);
                }
                
            }
        console.log("certif et secteur");
    }else if(Taille != 0 && certif != 0){
        finalRaisonsAxe11 = [];
        function test1(Raisone1){
            Raison_TotalByCErtif = [];
            
            
            for (let j = 0; j < x.length; j++) {
  
                for (let i = 0; i < uniqueReponse.length; i++) {
              
                    // Petite Entreprise
                    if(Taille == 1){
              
                        for (let k = 0; k < 6; k++) {
                   
                            if(x[j].QCertifications != null){
                               
                                if(x[j].QCertifications.split(",")[k] == certif){
                                    if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés" || x[j].QNBSalarie == "Entre 100 et 200 salariés"){ 
                                        if(uniqueReponse[i] == x[j][Raisone1]){
              
                                                if (!Raison_TotalByCErtif[uniqueReponse[i]]){
                                                    Raison_TotalByCErtif[uniqueReponse[i]] = 1;
                                                } 
                                                else Raison_TotalByCErtif[uniqueReponse[i]]++;
                                            }
                                    }
                                }
                
                            }
                        }
                    }
                    
                    // Grande Entreprise
                    if(Taille == 2){
              
                        for (let k = 0; k < 6; k++) {
                   
                            if(x[j].QCertifications != null){
                               
                                if(x[j].QCertifications.split(",")[k] == certif){
                                    if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
                                        if(uniqueReponse[i] == x[j][Raisone1]){
              
                                                if (!Raison_TotalByCErtif[uniqueReponse[i]]){
                                                    Raison_TotalByCErtif[uniqueReponse[i]] = 1;
                                                } 
                                                else Raison_TotalByCErtif[uniqueReponse[i]]++;
                                            }
                                    }
                                }
                
                            }
                        }
                    }
              
                }
                    // 
              }
        
            
        
            return Raison_TotalByCErtif;
            }

        // Les Raisons

        for (let i = 0; i < arrayRaison.length; i++) {
            finalRaisonsAxe11.push(test1(arrayRaison[i]));
            }
            
            
            for (let i = 0; i < finalRaisonsAxe11.length; i++) {
                if(finalRaisonsAxe11[i]["Moyennement d’accord"] == null){
                    MoyenDaccordAxe1.push(0);
                }else{
                    MoyenDaccordAxe1.push(finalRaisonsAxe11[i]["Moyennement d’accord"]);
                }
                if(finalRaisonsAxe11[i]["Plutôt d’accord"] == null){
                    plutotDacordAxe1.push(0);
                }else{
                    plutotDacordAxe1.push(finalRaisonsAxe11[i]["Plutôt d’accord"]);
                }
                if(finalRaisonsAxe11[i]["Tout à fait d’accord"] == null){
                    ToutAFDacordAxe1.push(0);
                }else{
                    ToutAFDacordAxe1.push(finalRaisonsAxe11[i]["Tout à fait d’accord"]);
                }
                if(finalRaisonsAxe11[i]["Plutôt pas d’accord"] == null){
                    plutotPasDacordAxe1.push(0);
                }else{
                    plutotPasDacordAxe1.push(finalRaisonsAxe11[i]["Plutôt pas d’accord"]);
                }
                if(finalRaisonsAxe11[i]["Pas du tout d’accord"] == null){
                    PasDeTDacordAxe1.push(0);
                }else{
                    PasDeTDacordAxe1.push(finalRaisonsAxe11[i]["Pas du tout d’accord"]);
                }
                
            }
        console.log("Taille certif");
    }else if(Taille != 0 && secteur != 0){
        finalRaisonsAxe11 = [];
        function test1(Raisone1){
            Raison_TotalBySecteur = [];
            
            
            for (let j = 0; j < x.length; j++) {
  
                for (let i = 0; i < uniqueReponse.length; i++) {
              
                    // // Petite Entreprise
                    if(Taille == 1){
                    if(x[j].QSecteur.toLowerCase().trim() == secteur){
                        if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés" || x[j].QNBSalarie == "Entre 100 et 200 salariés"){ 
                            if(uniqueReponse[i] == x[j][Raisone1]){
              
                                if (!Raison_TotalBySecteur[uniqueReponse[i]]){
                                    Raison_TotalBySecteur[uniqueReponse[i]] = 1;
                                } 
                                    else Raison_TotalBySecteur[uniqueReponse[i]]++;
                                }
                            }
                        }
                    }
                    // 
                    
                    // 
                     // // Grande Entreprise
                     if(Taille == 2){
                     if(x[j].QSecteur.toLowerCase().trim() == secteur){
                        if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
                            if(uniqueReponse[i] == x[j][Raisone1]){
              
                                if (!Raison_TotalBySecteur[uniqueReponse[i]]){
                                    Raison_TotalBySecteur[uniqueReponse[i]] = 1;
                                } 
                                    else Raison_TotalBySecteur[uniqueReponse[i]]++;
                                }
                            }
                        }
                    }
                    // 
                }
                    
              }
        
            
        
            return Raison_TotalBySecteur;
            }

        // Les Raisons

        for (let i = 0; i < arrayRaison.length; i++) {
            finalRaisonsAxe11.push(test1(arrayRaison[i]));
            }
            
            
            for (let i = 0; i < finalRaisonsAxe11.length; i++) {
                if(finalRaisonsAxe11[i]["Moyennement d’accord"] == null){
                    MoyenDaccordAxe1.push(0);
                }else{
                    MoyenDaccordAxe1.push(finalRaisonsAxe11[i]["Moyennement d’accord"]);
                }
                if(finalRaisonsAxe11[i]["Plutôt d’accord"] == null){
                    plutotDacordAxe1.push(0);
                }else{
                    plutotDacordAxe1.push(finalRaisonsAxe11[i]["Plutôt d’accord"]);
                }
                if(finalRaisonsAxe11[i]["Tout à fait d’accord"] == null){
                    ToutAFDacordAxe1.push(0);
                }else{
                    ToutAFDacordAxe1.push(finalRaisonsAxe11[i]["Tout à fait d’accord"]);
                }
                if(finalRaisonsAxe11[i]["Plutôt pas d’accord"] == null){
                    plutotPasDacordAxe1.push(0);
                }else{
                    plutotPasDacordAxe1.push(finalRaisonsAxe11[i]["Plutôt pas d’accord"]);
                }
                if(finalRaisonsAxe11[i]["Pas du tout d’accord"] == null){
                    PasDeTDacordAxe1.push(0);
                }else{
                    PasDeTDacordAxe1.push(finalRaisonsAxe11[i]["Pas du tout d’accord"]);
                }
                
            }
     console.log("1 and 2");
    }else if(Taille != 0){

        function test1(Raisone1){
            Raison_TotalAxe1 = [];
            if(Taille == 1){
            for (let j = 0; j < x.length; j++) {
              for (let i = 0; i < uniqueReponse.length; i++) {
                  
                        if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés" || x[j].QNBSalarie == "Entre 100 et 200 salariés"){ 
    
                        if(uniqueReponse[i] == x[j][Raisone1]){
                            if (!Raison_TotalAxe1[uniqueReponse[i]]){
                            Raison_TotalAxe1[uniqueReponse[i]] = 1;
                            } 
                            else Raison_TotalAxe1[uniqueReponse[i]]++;
                        }
                    }
                }
                
              }
           
            }
            if(Taille == 2){
                for (let j = 0; j < x.length; j++) {
                    for (let i = 0; i < uniqueReponse.length; i++) {
                        
                        if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
          
                              if(uniqueReponse[i] == x[j][Raisone1]){
                                  if (!Raison_TotalAxe1[uniqueReponse[i]]){
                                  Raison_TotalAxe1[uniqueReponse[i]] = 1;
                                  } 
                                  else Raison_TotalAxe1[uniqueReponse[i]]++;
                              }
                          }
                      }
                      
                    }
                
            }
            return Raison_TotalAxe1;
            }
    
    // Les Raisons
    
    for (let i = 0; i < arrayRaison.length; i++) {
        finalRaisonsAxe1.push(test1(arrayRaison[i]));
        }
        
        
        for (let i = 0; i < finalRaisonsAxe1.length; i++) {
            if(finalRaisonsAxe1[i]["Moyennement d’accord"] == null){
                MoyenDaccordAxe1.push(0);
            }else{
                MoyenDaccordAxe1.push(finalRaisonsAxe1[i]["Moyennement d’accord"]);
            }
            if(finalRaisonsAxe1[i]["Plutôt d’accord"] == null){
                plutotDacordAxe1.push(0);
            }else{
                plutotDacordAxe1.push(finalRaisonsAxe1[i]["Plutôt d’accord"]);
            }
            if(finalRaisonsAxe1[i]["Tout à fait d’accord"] == null){
                ToutAFDacordAxe1.push(0);
            }else{
                ToutAFDacordAxe1.push(finalRaisonsAxe1[i]["Tout à fait d’accord"]);
            }
            if(finalRaisonsAxe1[i]["Plutôt pas d’accord"] == null){
                plutotPasDacordAxe1.push(0);
            }else{
                plutotPasDacordAxe1.push(finalRaisonsAxe1[i]["Plutôt pas d’accord"]);
            }
            if(finalRaisonsAxe1[i]["Pas du tout d’accord"] == null){
                PasDeTDacordAxe1.push(0);
            }else{
                PasDeTDacordAxe1.push(finalRaisonsAxe1[i]["Pas du tout d’accord"]);
            }
            
        }
    
        // 
        console.log("1");
    //   End
    }else if(secteur != 0){

        function test1(Raisone1){
            Raison_TotalAxe1S = [];
            
            for (let j = 0; j < x.length; j++) {
                if(x[j].QSecteur.toLowerCase().trim() == secteur){
            for (let i = 0; i < uniqueReponse.length; i++) {
                
                
                        if(uniqueReponse[i] == x[j][Raisone1]){
                            if (!Raison_TotalAxe1S[uniqueReponse[i]]){
                            Raison_TotalAxe1S[uniqueReponse[i]] = 1;
                            } 
                            else Raison_TotalAxe1S[uniqueReponse[i]]++;
                        }
                    }

                }
                
            }
        
            
        
            return Raison_TotalAxe1S;
            }

        // Les Raisons

        for (let i = 0; i < arrayRaison.length; i++) {
            finalRaisonsAxe1.push(test1(arrayRaison[i]));
            }
            
            
            for (let i = 0; i < finalRaisonsAxe1.length; i++) {
                if(finalRaisonsAxe1[i]["Moyennement d’accord"] == null){
                    MoyenDaccordAxe1.push(0);
                }else{
                    MoyenDaccordAxe1.push(finalRaisonsAxe1[i]["Moyennement d’accord"]);
                }
                if(finalRaisonsAxe1[i]["Plutôt d’accord"] == null){
                    plutotDacordAxe1.push(0);
                }else{
                    plutotDacordAxe1.push(finalRaisonsAxe1[i]["Plutôt d’accord"]);
                }
                if(finalRaisonsAxe1[i]["Tout à fait d’accord"] == null){
                    ToutAFDacordAxe1.push(0);
                }else{
                    ToutAFDacordAxe1.push(finalRaisonsAxe1[i]["Tout à fait d’accord"]);
                }
                if(finalRaisonsAxe1[i]["Plutôt pas d’accord"] == null){
                    plutotPasDacordAxe1.push(0);
                }else{
                    plutotPasDacordAxe1.push(finalRaisonsAxe1[i]["Plutôt pas d’accord"]);
                }
                if(finalRaisonsAxe1[i]["Pas du tout d’accord"] == null){
                    PasDeTDacordAxe1.push(0);
                }else{
                    PasDeTDacordAxe1.push(finalRaisonsAxe1[i]["Pas du tout d’accord"]);
                }
                
            }

        // 
        console.log("2");
        //   End
    }else if(certif != 0){
    
    function test1(Raisone1){
        Raison_TotalAxe1C = [];
        
                for (let j = 0; j < x.length; j++) {
                    
                for (let i = 0; i < uniqueReponse.length; i++) {
                    for (let k = 0; k < 6; k++) {
            
                            if(x[j].QCertifications != null && x[j].QCertifications.split(",")[k] == certif){
                    
                            if(uniqueReponse[i] == x[j][Raisone1]){
                                if (!Raison_TotalAxe1C[uniqueReponse[i]]){
                                Raison_TotalAxe1C[uniqueReponse[i]] = 1;
                                } 
                                else Raison_TotalAxe1C[uniqueReponse[i]]++;
                            }
                        
                    }
                }
                    
            }
        }
                return Raison_TotalAxe1C;
                
        
        }
    
    // Les Raisons
    
    for (let i = 0; i < arrayRaison.length; i++) {
        finalRaisonsAxe1.push(test1(arrayRaison[i]));
        
        }
        
        
        for (let i = 0; i < finalRaisonsAxe1.length; i++) {
            if(finalRaisonsAxe1[i]["Moyennement d’accord"] == null){
                MoyenDaccordAxe1.push(0);
            }else{
                MoyenDaccordAxe1.push(finalRaisonsAxe1[i]["Moyennement d’accord"]);
            }
            if(finalRaisonsAxe1[i]["Plutôt d’accord"] == null){
                plutotDacordAxe1.push(0);
            }else{
                plutotDacordAxe1.push(finalRaisonsAxe1[i]["Plutôt d’accord"]);
            }
            if(finalRaisonsAxe1[i]["Tout à fait d’accord"] == null){
                ToutAFDacordAxe1.push(0);
            }else{
                ToutAFDacordAxe1.push(finalRaisonsAxe1[i]["Tout à fait d’accord"]);
            }
            if(finalRaisonsAxe1[i]["Plutôt pas d’accord"] == null){
                plutotPasDacordAxe1.push(0);
            }else{
                plutotPasDacordAxe1.push(finalRaisonsAxe1[i]["Plutôt pas d’accord"]);
            }
            if(finalRaisonsAxe1[i]["Pas du tout d’accord"] == null){
                PasDeTDacordAxe1.push(0);
            }else{
                PasDeTDacordAxe1.push(finalRaisonsAxe1[i]["Pas du tout d’accord"]);
            }
            console.log(finalRaisonsAxe1[i]);
        }
    
    // 
   
    //   End
    }
    
  // ---------------Raison---------------
  Highcharts.chart('containerAXE1123', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Histo. des réponses au sujet des raisons d\'adoptation d\'un systéme de management SST'
    },
    xAxis: {
        
        categories: arrayRaison,
        axisLabel: {
            interval: 0,
            rotate: 30 
          }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Réponses'
        }
    },
    tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>:  ({point.percentage:.0f}%)<br/>',
        shared: true
    },
    plotOptions: {
        column: {
            stacking: 'percent'
        }
    },
    series: [{
        name: 'Moyennement d’accord',
        data: MoyenDaccordAxe1
    }, {
        name: 'Plutôt d’accord',
        data: plutotDacordAxe1
    }, {
        name: 'Tout à fait d’accord',
        data: ToutAFDacordAxe1
    }, {
        name: 'Plutôt pas d’accord',
        data: plutotPasDacordAxe1
    }, {
        name: 'Pas du tout d’accord',
        data: PasDeTDacordAxe1
    }]
});
     
}

    // Axe 2 start
    function axes2(){

        Taille = document.getElementById('TailleAxe2123').value;
        secteur = document.getElementById('secteurax2123').value;
        certif = document.getElementById('CertificasAx2123').value;
    
        var MoyenDaccordAxe1=[],plutotDacordAxe1=[],ToutAFDacordAxe1=[],plutotPasDacordAxe1=[],PasDeTDacordAxe1=[],finalRaisonsAxe1=[];
        if(Taille != 0 && secteur != 0 && certif != 0){
            finalRaisonsAxe11 = [];
            function test1(Raisone1){
                Raison_TotalByCErtif = [];
                
                
                for (let j = 0; j < x.length; j++) {
      
                    for (let i = 0; i < uniqueReponse.length; i++) {
                  
                        // Petite Entreprise
                        if(Taille == 1){
                  
                            for (let k = 0; k < 6; k++) {
                       
                                if(x[j].QCertifications != null){
                                   
                                    if(x[j].QCertifications.split(",")[k] == certif){
                                        if(x[j].QSecteur.toLowerCase().trim() == secteur){
                                        if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés" || x[j].QNBSalarie == "Entre 100 et 200 salariés"){ 
                                            if(uniqueReponse[i] == x[j][Raisone1]){
                  
                                                    if (!Raison_TotalByCErtif[uniqueReponse[i]]){
                                                        Raison_TotalByCErtif[uniqueReponse[i]] = 1;
                                                    } 
                                                    else Raison_TotalByCErtif[uniqueReponse[i]]++;
                                                }
                                        }
                                    }
                                }
                    
                                }
                            }
                        }
                        
                        // Grande Entreprise
                        if(Taille == 2){
                  
                            for (let k = 0; k < 6; k++) {
                       
                                if(x[j].QCertifications != null){
                                   
                                    if(x[j].QCertifications.split(",")[k] == certif){
                                        if(x[j].QSecteur.toLowerCase().trim() == secteur){
                                        if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
                                            if(uniqueReponse[i] == x[j][Raisone1]){
                  
                                                    if (!Raison_TotalByCErtif[uniqueReponse[i]]){
                                                        Raison_TotalByCErtif[uniqueReponse[i]] = 1;
                                                    } 
                                                    else Raison_TotalByCErtif[uniqueReponse[i]]++;
                                                }
                                        }
                                    }
                                }
                    
                                }
                            }
                        }
                  
                    }
                        // 
                  }
            
                
            
                return Raison_TotalByCErtif;
                }
    
            // Les Raisons
    
            for (let i = 0; i < arrayObstacle.length; i++) {
                finalRaisonsAxe11.push(test1(arrayObstacle[i]));
                }
                
                
                for (let i = 0; i < finalRaisonsAxe11.length; i++) {
                    if(finalRaisonsAxe11[i]["Moyennement d’accord"] == null){
                        MoyenDaccordAxe1.push(0);
                    }else{
                        MoyenDaccordAxe1.push(finalRaisonsAxe11[i]["Moyennement d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Plutôt d’accord"] == null){
                        plutotDacordAxe1.push(0);
                    }else{
                        plutotDacordAxe1.push(finalRaisonsAxe11[i]["Plutôt d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Tout à fait d’accord"] == null){
                        ToutAFDacordAxe1.push(0);
                    }else{
                        ToutAFDacordAxe1.push(finalRaisonsAxe11[i]["Tout à fait d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Plutôt pas d’accord"] == null){
                        plutotPasDacordAxe1.push(0);
                    }else{
                        plutotPasDacordAxe1.push(finalRaisonsAxe11[i]["Plutôt pas d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Pas du tout d’accord"] == null){
                        PasDeTDacordAxe1.push(0);
                    }else{
                        PasDeTDacordAxe1.push(finalRaisonsAxe11[i]["Pas du tout d’accord"]);
                    }
                    
                }
            console.log("Taille,'-',secteur,'-',certif");
        }else if(certif != 0 && secteur != 0){
            finalRaisonsAxe11 = [];
            function test1(Raisone1){
                Raison_TotalByCErtif = [];
                
                
                for (let j = 0; j < x.length; j++) {
      
                    for (let i = 0; i < uniqueReponse.length; i++) {
                  
                        // Petite Entreprise
                        
                  
                            for (let k = 0; k < 6; k++) {
                       
                                if(x[j].QCertifications != null){
                                   
                                    if(x[j].QCertifications.split(",")[k] == certif){
                                        if(x[j].QSecteur.toLowerCase().trim() == secteur){
                                            if(uniqueReponse[i] == x[j][Raisone1]){
                  
                                                    if (!Raison_TotalByCErtif[uniqueReponse[i]]){
                                                        Raison_TotalByCErtif[uniqueReponse[i]] = 1;
                                                    } 
                                                    else Raison_TotalByCErtif[uniqueReponse[i]]++;
                                                }
                                        }
                                    }
                    
                                }
                            }
                    }
                        // 
                  }
            
                
            
                return Raison_TotalByCErtif;
                }
    
            // Les Raisons
    
            for (let i = 0; i < arrayObstacle.length; i++) {
                finalRaisonsAxe11.push(test1(arrayObstacle[i]));
                }
                
                
                for (let i = 0; i < finalRaisonsAxe11.length; i++) {
                    if(finalRaisonsAxe11[i]["Moyennement d’accord"] == null){
                        MoyenDaccordAxe1.push(0);
                    }else{
                        MoyenDaccordAxe1.push(finalRaisonsAxe11[i]["Moyennement d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Plutôt d’accord"] == null){
                        plutotDacordAxe1.push(0);
                    }else{
                        plutotDacordAxe1.push(finalRaisonsAxe11[i]["Plutôt d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Tout à fait d’accord"] == null){
                        ToutAFDacordAxe1.push(0);
                    }else{
                        ToutAFDacordAxe1.push(finalRaisonsAxe11[i]["Tout à fait d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Plutôt pas d’accord"] == null){
                        plutotPasDacordAxe1.push(0);
                    }else{
                        plutotPasDacordAxe1.push(finalRaisonsAxe11[i]["Plutôt pas d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Pas du tout d’accord"] == null){
                        PasDeTDacordAxe1.push(0);
                    }else{
                        PasDeTDacordAxe1.push(finalRaisonsAxe11[i]["Pas du tout d’accord"]);
                    }
                    
                }
            console.log("certif et secteur");
        }else if(Taille != 0 && certif != 0){
            finalRaisonsAxe11 = [];
            function test1(Raisone1){
                Raison_TotalByCErtif = [];
                
                
                for (let j = 0; j < x.length; j++) {
      
                    for (let i = 0; i < uniqueReponse.length; i++) {
                  
                        // Petite Entreprise
                        if(Taille == 1){
                  
                            for (let k = 0; k < 6; k++) {
                       
                                if(x[j].QCertifications != null){
                                   
                                    if(x[j].QCertifications.split(",")[k] == certif){
                                        if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés" || x[j].QNBSalarie == "Entre 100 et 200 salariés"){ 
                                            if(uniqueReponse[i] == x[j][Raisone1]){
                  
                                                    if (!Raison_TotalByCErtif[uniqueReponse[i]]){
                                                        Raison_TotalByCErtif[uniqueReponse[i]] = 1;
                                                    } 
                                                    else Raison_TotalByCErtif[uniqueReponse[i]]++;
                                                }
                                        }
                                    }
                    
                                }
                            }
                        }
                        
                        // Grande Entreprise
                        if(Taille == 2){
                  
                            for (let k = 0; k < 6; k++) {
                       
                                if(x[j].QCertifications != null){
                                   
                                    if(x[j].QCertifications.split(",")[k] == certif){
                                        if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
                                            if(uniqueReponse[i] == x[j][Raisone1]){
                  
                                                    if (!Raison_TotalByCErtif[uniqueReponse[i]]){
                                                        Raison_TotalByCErtif[uniqueReponse[i]] = 1;
                                                    } 
                                                    else Raison_TotalByCErtif[uniqueReponse[i]]++;
                                                }
                                        }
                                    }
                    
                                }
                            }
                        }
                  
                    }
                        // 
                  }
            
                
            
                return Raison_TotalByCErtif;
                }
    
            // Les Raisons
    
            for (let i = 0; i < arrayObstacle.length; i++) {
                finalRaisonsAxe11.push(test1(arrayObstacle[i]));
                }
                
                
                for (let i = 0; i < finalRaisonsAxe11.length; i++) {
                    if(finalRaisonsAxe11[i]["Moyennement d’accord"] == null){
                        MoyenDaccordAxe1.push(0);
                    }else{
                        MoyenDaccordAxe1.push(finalRaisonsAxe11[i]["Moyennement d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Plutôt d’accord"] == null){
                        plutotDacordAxe1.push(0);
                    }else{
                        plutotDacordAxe1.push(finalRaisonsAxe11[i]["Plutôt d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Tout à fait d’accord"] == null){
                        ToutAFDacordAxe1.push(0);
                    }else{
                        ToutAFDacordAxe1.push(finalRaisonsAxe11[i]["Tout à fait d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Plutôt pas d’accord"] == null){
                        plutotPasDacordAxe1.push(0);
                    }else{
                        plutotPasDacordAxe1.push(finalRaisonsAxe11[i]["Plutôt pas d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Pas du tout d’accord"] == null){
                        PasDeTDacordAxe1.push(0);
                    }else{
                        PasDeTDacordAxe1.push(finalRaisonsAxe11[i]["Pas du tout d’accord"]);
                    }
                    
                }
            console.log("Taille certif");
        }else if(Taille != 0 && secteur != 0){
            finalRaisonsAxe11 = [];
            function test1(Raisone1){
                Raison_TotalBySecteur = [];
                
                
                for (let j = 0; j < x.length; j++) {
      
                    for (let i = 0; i < uniqueReponse.length; i++) {
                  
                        // // Petite Entreprise
                        if(Taille == 1){
                        if(x[j].QSecteur.toLowerCase().trim() == secteur){
                            if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés" || x[j].QNBSalarie == "Entre 100 et 200 salariés"){ 
                                if(uniqueReponse[i] == x[j][Raisone1]){
                  
                                    if (!Raison_TotalBySecteur[uniqueReponse[i]]){
                                        Raison_TotalBySecteur[uniqueReponse[i]] = 1;
                                    } 
                                        else Raison_TotalBySecteur[uniqueReponse[i]]++;
                                    }
                                }
                            }
                        }
                        // 
                        
                        // 
                         // // Grande Entreprise
                         if(Taille == 2){
                         if(x[j].QSecteur.toLowerCase().trim() == secteur){
                            if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
                                if(uniqueReponse[i] == x[j][Raisone1]){
                  
                                    if (!Raison_TotalBySecteur[uniqueReponse[i]]){
                                        Raison_TotalBySecteur[uniqueReponse[i]] = 1;
                                    } 
                                        else Raison_TotalBySecteur[uniqueReponse[i]]++;
                                    }
                                }
                            }
                        }
                        // 
                    }
                        
                  }

                return Raison_TotalBySecteur;
                }
    
            // Les Raisons
    
            for (let i = 0; i < arrayObstacle.length; i++) {
                finalRaisonsAxe11.push(test1(arrayObstacle[i]));
                }
                
                
                for (let i = 0; i < finalRaisonsAxe11.length; i++) {
                    if(finalRaisonsAxe11[i]["Moyennement d’accord"] == null){
                        MoyenDaccordAxe1.push(0);
                    }else{
                        MoyenDaccordAxe1.push(finalRaisonsAxe11[i]["Moyennement d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Plutôt d’accord"] == null){
                        plutotDacordAxe1.push(0);
                    }else{
                        plutotDacordAxe1.push(finalRaisonsAxe11[i]["Plutôt d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Tout à fait d’accord"] == null){
                        ToutAFDacordAxe1.push(0);
                    }else{
                        ToutAFDacordAxe1.push(finalRaisonsAxe11[i]["Tout à fait d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Plutôt pas d’accord"] == null){
                        plutotPasDacordAxe1.push(0);
                    }else{
                        plutotPasDacordAxe1.push(finalRaisonsAxe11[i]["Plutôt pas d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Pas du tout d’accord"] == null){
                        PasDeTDacordAxe1.push(0);
                    }else{
                        PasDeTDacordAxe1.push(finalRaisonsAxe11[i]["Pas du tout d’accord"]);
                    }
                    
                }
         console.log("1 and 2");
        }else if(Taille != 0){
    
            function test1(Raisone1){
                Raison_TotalAxe1 = [];
                if(Taille == 1){
                for (let j = 0; j < x.length; j++) {
                  for (let i = 0; i < uniqueReponse.length; i++) {
                      
                            if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés" || x[j].QNBSalarie == "Entre 100 et 200 salariés"){ 
        
                            if(uniqueReponse[i] == x[j][Raisone1]){
                                if (!Raison_TotalAxe1[uniqueReponse[i]]){
                                Raison_TotalAxe1[uniqueReponse[i]] = 1;
                                } 
                                else Raison_TotalAxe1[uniqueReponse[i]]++;
                            }
                        }
                    }
                    
                  }
               
                }
                if(Taille == 2){
                    for (let j = 0; j < x.length; j++) {
                        for (let i = 0; i < uniqueReponse.length; i++) {
                            
                            if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
              
                                  if(uniqueReponse[i] == x[j][Raisone1]){
                                      if (!Raison_TotalAxe1[uniqueReponse[i]]){
                                      Raison_TotalAxe1[uniqueReponse[i]] = 1;
                                      } 
                                      else Raison_TotalAxe1[uniqueReponse[i]]++;
                                  }
                              }
                          }
                          
                        }
                    
                }
                return Raison_TotalAxe1;
                }
        
        // Les Raisons
        
        for (let i = 0; i < arrayObstacle.length; i++) {
            finalRaisonsAxe1.push(test1(arrayObstacle[i]));
            }
            
            
            for (let i = 0; i < finalRaisonsAxe1.length; i++) {
                if(finalRaisonsAxe1[i]["Moyennement d’accord"] == null){
                    MoyenDaccordAxe1.push(0);
                }else{
                    MoyenDaccordAxe1.push(finalRaisonsAxe1[i]["Moyennement d’accord"]);
                }
                if(finalRaisonsAxe1[i]["Plutôt d’accord"] == null){
                    plutotDacordAxe1.push(0);
                }else{
                    plutotDacordAxe1.push(finalRaisonsAxe1[i]["Plutôt d’accord"]);
                }
                if(finalRaisonsAxe1[i]["Tout à fait d’accord"] == null){
                    ToutAFDacordAxe1.push(0);
                }else{
                    ToutAFDacordAxe1.push(finalRaisonsAxe1[i]["Tout à fait d’accord"]);
                }
                if(finalRaisonsAxe1[i]["Plutôt pas d’accord"] == null){
                    plutotPasDacordAxe1.push(0);
                }else{
                    plutotPasDacordAxe1.push(finalRaisonsAxe1[i]["Plutôt pas d’accord"]);
                }
                if(finalRaisonsAxe1[i]["Pas du tout d’accord"] == null){
                    PasDeTDacordAxe1.push(0);
                }else{
                    PasDeTDacordAxe1.push(finalRaisonsAxe1[i]["Pas du tout d’accord"]);
                }
                
            }
        
            // 
            console.log("1");
        //   End
        }else if(secteur != 0){
    
            function test1(Raisone1){
                Raison_TotalAxe1S = [];
                
                for (let j = 0; j < x.length; j++) {
                    if(x[j].QSecteur.toLowerCase().trim() == secteur){
                for (let i = 0; i < uniqueReponse.length; i++) {
                    
                    
                            if(uniqueReponse[i] == x[j][Raisone1]){
                                if (!Raison_TotalAxe1S[uniqueReponse[i]]){
                                Raison_TotalAxe1S[uniqueReponse[i]] = 1;
                                } 
                                else Raison_TotalAxe1S[uniqueReponse[i]]++;
                            }
                        }
    
                    }
                    
                }
            
                
            
                return Raison_TotalAxe1S;
                }
    
            // Les Raisons
    
            for (let i = 0; i < arrayObstacle.length; i++) {
                finalRaisonsAxe1.push(test1(arrayObstacle[i]));
                }
                
                
                for (let i = 0; i < finalRaisonsAxe1.length; i++) {
                    if(finalRaisonsAxe1[i]["Moyennement d’accord"] == null){
                        MoyenDaccordAxe1.push(0);
                    }else{
                        MoyenDaccordAxe1.push(finalRaisonsAxe1[i]["Moyennement d’accord"]);
                    }
                    if(finalRaisonsAxe1[i]["Plutôt d’accord"] == null){
                        plutotDacordAxe1.push(0);
                    }else{
                        plutotDacordAxe1.push(finalRaisonsAxe1[i]["Plutôt d’accord"]);
                    }
                    if(finalRaisonsAxe1[i]["Tout à fait d’accord"] == null){
                        ToutAFDacordAxe1.push(0);
                    }else{
                        ToutAFDacordAxe1.push(finalRaisonsAxe1[i]["Tout à fait d’accord"]);
                    }
                    if(finalRaisonsAxe1[i]["Plutôt pas d’accord"] == null){
                        plutotPasDacordAxe1.push(0);
                    }else{
                        plutotPasDacordAxe1.push(finalRaisonsAxe1[i]["Plutôt pas d’accord"]);
                    }
                    if(finalRaisonsAxe1[i]["Pas du tout d’accord"] == null){
                        PasDeTDacordAxe1.push(0);
                    }else{
                        PasDeTDacordAxe1.push(finalRaisonsAxe1[i]["Pas du tout d’accord"]);
                    }
                    
                }
    
            // 
            console.log("2");
            //   End
        }else if(certif != 0){
        
        function test1(Raisone1){
            Raison_TotalAxe1C = [];
            
                    for (let j = 0; j < x.length; j++) {
                        
                    for (let i = 0; i < uniqueReponse.length; i++) {
                        for (let k = 0; k < 6; k++) {
                
                                if(x[j].QCertifications != null && x[j].QCertifications.split(",")[k] == certif){
                        
                                if(uniqueReponse[i] == x[j][Raisone1]){
                                    if (!Raison_TotalAxe1C[uniqueReponse[i]]){
                                    Raison_TotalAxe1C[uniqueReponse[i]] = 1;
                                    } 
                                    else Raison_TotalAxe1C[uniqueReponse[i]]++;
                                }
                            
                        }
                    }
                        
                }
            }
                    return Raison_TotalAxe1C;
                    
            
            }
        
        // Les Raisons
        
        for (let i = 0; i < arrayObstacle.length; i++) {
            finalRaisonsAxe1.push(test1(arrayObstacle[i]));
            
            }
            
            
            for (let i = 0; i < finalRaisonsAxe1.length; i++) {
                if(finalRaisonsAxe1[i]["Moyennement d’accord"] == null){
                    MoyenDaccordAxe1.push(0);
                }else{
                    MoyenDaccordAxe1.push(finalRaisonsAxe1[i]["Moyennement d’accord"]);
                }
                if(finalRaisonsAxe1[i]["Plutôt d’accord"] == null){
                    plutotDacordAxe1.push(0);
                }else{
                    plutotDacordAxe1.push(finalRaisonsAxe1[i]["Plutôt d’accord"]);
                }
                if(finalRaisonsAxe1[i]["Tout à fait d’accord"] == null){
                    ToutAFDacordAxe1.push(0);
                }else{
                    ToutAFDacordAxe1.push(finalRaisonsAxe1[i]["Tout à fait d’accord"]);
                }
                if(finalRaisonsAxe1[i]["Plutôt pas d’accord"] == null){
                    plutotPasDacordAxe1.push(0);
                }else{
                    plutotPasDacordAxe1.push(finalRaisonsAxe1[i]["Plutôt pas d’accord"]);
                }
                if(finalRaisonsAxe1[i]["Pas du tout d’accord"] == null){
                    PasDeTDacordAxe1.push(0);
                }else{
                    PasDeTDacordAxe1.push(finalRaisonsAxe1[i]["Pas du tout d’accord"]);
                }
                console.log(finalRaisonsAxe1[i]);
            }
        
        // 
       
        //   End
        }
        
      // ---------------Raison---------------
      Highcharts.chart('containerAXE2123', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Histo. des réponses au sujet des raisons d\'adoptation d\'un systéme de management SST'
        },
        xAxis: {
            
            categories: arrayObstacle,
            axisLabel: {
                interval: 0,
                rotate: 30 
              }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Réponses'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>:  ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'percent'
            }
        },
        series: [{
            name: 'Moyennement d’accord',
            data: MoyenDaccordAxe1
        }, {
            name: 'Plutôt d’accord',
            data: plutotDacordAxe1
        }, {
            name: 'Tout à fait d’accord',
            data: ToutAFDacordAxe1
        }, {
            name: 'Plutôt pas d’accord',
            data: plutotPasDacordAxe1
        }, {
            name: 'Pas du tout d’accord',
            data: PasDeTDacordAxe1
        }]
    });
         
    }
    
    // Axe 2 ENd
    // Axe 3 start
    function axes3(){

        Taille = document.getElementById('TailleAxe3123').value;
        secteur = document.getElementById('secteurax3123').value;
        certif = document.getElementById('CertificasAx3123').value;
    
        var MoyenDaccordAxe1=[],plutotDacordAxe1=[],ToutAFDacordAxe1=[],plutotPasDacordAxe1=[],PasDeTDacordAxe1=[],finalRaisonsAxe1=[];
        if(Taille != 0 && secteur != 0 && certif != 0){
            finalRaisonsAxe11 = [];
            function test1(Raisone1){
                Raison_TotalByCErtif = [];
                
                
                for (let j = 0; j < x.length; j++) {
      
                    for (let i = 0; i < uniqueReponse.length; i++) {
                  
                        // Petite Entreprise
                        if(Taille == 1){
                  
                            for (let k = 0; k < 6; k++) {
                       
                                if(x[j].QCertifications != null){
                                   
                                    if(x[j].QCertifications.split(",")[k] == certif){
                                        if(x[j].QSecteur.toLowerCase().trim() == secteur){
                                        if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés" || x[j].QNBSalarie == "Entre 100 et 200 salariés"){ 
                                            if(uniqueReponse[i] == x[j][Raisone1]){
                  
                                                    if (!Raison_TotalByCErtif[uniqueReponse[i]]){
                                                        Raison_TotalByCErtif[uniqueReponse[i]] = 1;
                                                    } 
                                                    else Raison_TotalByCErtif[uniqueReponse[i]]++;
                                                }
                                        }
                                    }
                                }
                    
                                }
                            }
                        }
                        
                        // Grande Entreprise
                        if(Taille == 2){
                  
                            for (let k = 0; k < 6; k++) {
                       
                                if(x[j].QCertifications != null){
                                   
                                    if(x[j].QCertifications.split(",")[k] == certif){
                                        if(x[j].QSecteur.toLowerCase().trim() == secteur){
                                        if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
                                            if(uniqueReponse[i] == x[j][Raisone1]){
                  
                                                    if (!Raison_TotalByCErtif[uniqueReponse[i]]){
                                                        Raison_TotalByCErtif[uniqueReponse[i]] = 1;
                                                    } 
                                                    else Raison_TotalByCErtif[uniqueReponse[i]]++;
                                                }
                                        }
                                    }
                                }
                    
                                }
                            }
                        }
                  
                    }
                        // 
                  }
            
                
            
                return Raison_TotalByCErtif;
                }
    
            // Les Raisons
    
            for (let i = 0; i < arrayDifficultes.length; i++) {
                finalRaisonsAxe11.push(test1(arrayDifficultes[i]));
                }
                
                
                for (let i = 0; i < finalRaisonsAxe11.length; i++) {
                    if(finalRaisonsAxe11[i]["Moyennement d’accord"] == null){
                        MoyenDaccordAxe1.push(0);
                    }else{
                        MoyenDaccordAxe1.push(finalRaisonsAxe11[i]["Moyennement d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Plutôt d’accord"] == null){
                        plutotDacordAxe1.push(0);
                    }else{
                        plutotDacordAxe1.push(finalRaisonsAxe11[i]["Plutôt d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Tout à fait d’accord"] == null){
                        ToutAFDacordAxe1.push(0);
                    }else{
                        ToutAFDacordAxe1.push(finalRaisonsAxe11[i]["Tout à fait d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Plutôt pas d’accord"] == null){
                        plutotPasDacordAxe1.push(0);
                    }else{
                        plutotPasDacordAxe1.push(finalRaisonsAxe11[i]["Plutôt pas d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Pas du tout d’accord"] == null){
                        PasDeTDacordAxe1.push(0);
                    }else{
                        PasDeTDacordAxe1.push(finalRaisonsAxe11[i]["Pas du tout d’accord"]);
                    }
                    
                }
            console.log("Taille,'-',secteur,'-',certif");
        }else if(certif != 0 && secteur != 0){
            finalRaisonsAxe11 = [];
            function test1(Raisone1){
                Raison_TotalByCErtif = [];
                
                
                for (let j = 0; j < x.length; j++) {
      
                    for (let i = 0; i < uniqueReponse.length; i++) {
                  
                        // Petite Entreprise
                        
                  
                            for (let k = 0; k < 6; k++) {
                       
                                if(x[j].QCertifications != null){
                                   
                                    if(x[j].QCertifications.split(",")[k] == certif){
                                        if(x[j].QSecteur.toLowerCase().trim() == secteur){
                                            if(uniqueReponse[i] == x[j][Raisone1]){
                  
                                                    if (!Raison_TotalByCErtif[uniqueReponse[i]]){
                                                        Raison_TotalByCErtif[uniqueReponse[i]] = 1;
                                                    } 
                                                    else Raison_TotalByCErtif[uniqueReponse[i]]++;
                                                }
                                        }
                                    }
                    
                                }
                            }
                    }
                        // 
                  }
            
                
            
                return Raison_TotalByCErtif;
                }
    
            // Les Raisons
    
            for (let i = 0; i < arrayDifficultes.length; i++) {
                finalRaisonsAxe11.push(test1(arrayDifficultes[i]));
                }
                
                
                for (let i = 0; i < finalRaisonsAxe11.length; i++) {
                    if(finalRaisonsAxe11[i]["Moyennement d’accord"] == null){
                        MoyenDaccordAxe1.push(0);
                    }else{
                        MoyenDaccordAxe1.push(finalRaisonsAxe11[i]["Moyennement d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Plutôt d’accord"] == null){
                        plutotDacordAxe1.push(0);
                    }else{
                        plutotDacordAxe1.push(finalRaisonsAxe11[i]["Plutôt d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Tout à fait d’accord"] == null){
                        ToutAFDacordAxe1.push(0);
                    }else{
                        ToutAFDacordAxe1.push(finalRaisonsAxe11[i]["Tout à fait d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Plutôt pas d’accord"] == null){
                        plutotPasDacordAxe1.push(0);
                    }else{
                        plutotPasDacordAxe1.push(finalRaisonsAxe11[i]["Plutôt pas d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Pas du tout d’accord"] == null){
                        PasDeTDacordAxe1.push(0);
                    }else{
                        PasDeTDacordAxe1.push(finalRaisonsAxe11[i]["Pas du tout d’accord"]);
                    }
                    
                }
            console.log("certif et secteur");
        }else if(Taille != 0 && certif != 0){
            finalRaisonsAxe11 = [];
            function test1(Raisone1){
                Raison_TotalByCErtif = [];
                
                
                for (let j = 0; j < x.length; j++) {
      
                    for (let i = 0; i < uniqueReponse.length; i++) {
                  
                        // Petite Entreprise
                        if(Taille == 1){
                  
                            for (let k = 0; k < 6; k++) {
                       
                                if(x[j].QCertifications != null){
                                   
                                    if(x[j].QCertifications.split(",")[k] == certif){
                                        if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés" || x[j].QNBSalarie == "Entre 100 et 200 salariés"){ 
                                            if(uniqueReponse[i] == x[j][Raisone1]){
                  
                                                    if (!Raison_TotalByCErtif[uniqueReponse[i]]){
                                                        Raison_TotalByCErtif[uniqueReponse[i]] = 1;
                                                    } 
                                                    else Raison_TotalByCErtif[uniqueReponse[i]]++;
                                                }
                                        }
                                    }
                    
                                }
                            }
                        }
                        
                        // Grande Entreprise
                        if(Taille == 2){
                  
                            for (let k = 0; k < 6; k++) {
                       
                                if(x[j].QCertifications != null){
                                   
                                    if(x[j].QCertifications.split(",")[k] == certif){
                                        if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
                                            if(uniqueReponse[i] == x[j][Raisone1]){
                  
                                                    if (!Raison_TotalByCErtif[uniqueReponse[i]]){
                                                        Raison_TotalByCErtif[uniqueReponse[i]] = 1;
                                                    } 
                                                    else Raison_TotalByCErtif[uniqueReponse[i]]++;
                                                }
                                        }
                                    }
                    
                                }
                            }
                        }
                  
                    }
                        // 
                  }
            
                
            
                return Raison_TotalByCErtif;
                }
    
            // Les Raisons
    
            for (let i = 0; i < arrayDifficultes.length; i++) {
                finalRaisonsAxe11.push(test1(arrayDifficultes[i]));
                }
                
                
                for (let i = 0; i < finalRaisonsAxe11.length; i++) {
                    if(finalRaisonsAxe11[i]["Moyennement d’accord"] == null){
                        MoyenDaccordAxe1.push(0);
                    }else{
                        MoyenDaccordAxe1.push(finalRaisonsAxe11[i]["Moyennement d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Plutôt d’accord"] == null){
                        plutotDacordAxe1.push(0);
                    }else{
                        plutotDacordAxe1.push(finalRaisonsAxe11[i]["Plutôt d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Tout à fait d’accord"] == null){
                        ToutAFDacordAxe1.push(0);
                    }else{
                        ToutAFDacordAxe1.push(finalRaisonsAxe11[i]["Tout à fait d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Plutôt pas d’accord"] == null){
                        plutotPasDacordAxe1.push(0);
                    }else{
                        plutotPasDacordAxe1.push(finalRaisonsAxe11[i]["Plutôt pas d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Pas du tout d’accord"] == null){
                        PasDeTDacordAxe1.push(0);
                    }else{
                        PasDeTDacordAxe1.push(finalRaisonsAxe11[i]["Pas du tout d’accord"]);
                    }
                    
                }
            console.log("Taille certif");
        }else if(Taille != 0 && secteur != 0){
            finalRaisonsAxe11 = [];
            function test1(Raisone1){
                Raison_TotalBySecteur = [];
                
                
                for (let j = 0; j < x.length; j++) {
      
                    for (let i = 0; i < uniqueReponse.length; i++) {
                  
                        // // Petite Entreprise
                        if(Taille == 1){
                        if(x[j].QSecteur.toLowerCase().trim() == secteur){
                            if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés" || x[j].QNBSalarie == "Entre 100 et 200 salariés"){ 
                                if(uniqueReponse[i] == x[j][Raisone1]){
                  
                                    if (!Raison_TotalBySecteur[uniqueReponse[i]]){
                                        Raison_TotalBySecteur[uniqueReponse[i]] = 1;
                                    } 
                                        else Raison_TotalBySecteur[uniqueReponse[i]]++;
                                    }
                                }
                            }
                        }
                        // 
                        
                        // 
                         // // Grande Entreprise
                         if(Taille == 2){
                         if(x[j].QSecteur.toLowerCase().trim() == secteur){
                            if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
                                if(uniqueReponse[i] == x[j][Raisone1]){
                  
                                    if (!Raison_TotalBySecteur[uniqueReponse[i]]){
                                        Raison_TotalBySecteur[uniqueReponse[i]] = 1;
                                    } 
                                        else Raison_TotalBySecteur[uniqueReponse[i]]++;
                                    }
                                }
                            }
                        }
                        // 
                    }
                        
                  }

                return Raison_TotalBySecteur;
                }
    
            // Les Raisons
    
            for (let i = 0; i < arrayDifficultes.length; i++) {
                finalRaisonsAxe11.push(test1(arrayDifficultes[i]));
                }
                
                
                for (let i = 0; i < finalRaisonsAxe11.length; i++) {
                    if(finalRaisonsAxe11[i]["Moyennement d’accord"] == null){
                        MoyenDaccordAxe1.push(0);
                    }else{
                        MoyenDaccordAxe1.push(finalRaisonsAxe11[i]["Moyennement d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Plutôt d’accord"] == null){
                        plutotDacordAxe1.push(0);
                    }else{
                        plutotDacordAxe1.push(finalRaisonsAxe11[i]["Plutôt d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Tout à fait d’accord"] == null){
                        ToutAFDacordAxe1.push(0);
                    }else{
                        ToutAFDacordAxe1.push(finalRaisonsAxe11[i]["Tout à fait d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Plutôt pas d’accord"] == null){
                        plutotPasDacordAxe1.push(0);
                    }else{
                        plutotPasDacordAxe1.push(finalRaisonsAxe11[i]["Plutôt pas d’accord"]);
                    }
                    if(finalRaisonsAxe11[i]["Pas du tout d’accord"] == null){
                        PasDeTDacordAxe1.push(0);
                    }else{
                        PasDeTDacordAxe1.push(finalRaisonsAxe11[i]["Pas du tout d’accord"]);
                    }
                    
                }
         console.log("1 and 2");
        }else if(Taille != 0){
    
            function test1(Raisone1){
                Raison_TotalAxe1 = [];
                if(Taille == 1){
                for (let j = 0; j < x.length; j++) {
                  for (let i = 0; i < uniqueReponse.length; i++) {
                      
                            if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés" || x[j].QNBSalarie == "Entre 100 et 200 salariés"){ 
        
                            if(uniqueReponse[i] == x[j][Raisone1]){
                                if (!Raison_TotalAxe1[uniqueReponse[i]]){
                                Raison_TotalAxe1[uniqueReponse[i]] = 1;
                                } 
                                else Raison_TotalAxe1[uniqueReponse[i]]++;
                            }
                        }
                    }
                    
                  }
               
                }
                if(Taille == 2){
                    for (let j = 0; j < x.length; j++) {
                        for (let i = 0; i < uniqueReponse.length; i++) {
                            
                            if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
              
                                  if(uniqueReponse[i] == x[j][Raisone1]){
                                      if (!Raison_TotalAxe1[uniqueReponse[i]]){
                                      Raison_TotalAxe1[uniqueReponse[i]] = 1;
                                      } 
                                      else Raison_TotalAxe1[uniqueReponse[i]]++;
                                  }
                              }
                          }
                          
                        }
                    
                }
                return Raison_TotalAxe1;
                }
        
        // Les Raisons
        
        for (let i = 0; i < arrayDifficultes.length; i++) {
            finalRaisonsAxe1.push(test1(arrayDifficultes[i]));
            }
            
            
            for (let i = 0; i < finalRaisonsAxe1.length; i++) {
                if(finalRaisonsAxe1[i]["Moyennement d’accord"] == null){
                    MoyenDaccordAxe1.push(0);
                }else{
                    MoyenDaccordAxe1.push(finalRaisonsAxe1[i]["Moyennement d’accord"]);
                }
                if(finalRaisonsAxe1[i]["Plutôt d’accord"] == null){
                    plutotDacordAxe1.push(0);
                }else{
                    plutotDacordAxe1.push(finalRaisonsAxe1[i]["Plutôt d’accord"]);
                }
                if(finalRaisonsAxe1[i]["Tout à fait d’accord"] == null){
                    ToutAFDacordAxe1.push(0);
                }else{
                    ToutAFDacordAxe1.push(finalRaisonsAxe1[i]["Tout à fait d’accord"]);
                }
                if(finalRaisonsAxe1[i]["Plutôt pas d’accord"] == null){
                    plutotPasDacordAxe1.push(0);
                }else{
                    plutotPasDacordAxe1.push(finalRaisonsAxe1[i]["Plutôt pas d’accord"]);
                }
                if(finalRaisonsAxe1[i]["Pas du tout d’accord"] == null){
                    PasDeTDacordAxe1.push(0);
                }else{
                    PasDeTDacordAxe1.push(finalRaisonsAxe1[i]["Pas du tout d’accord"]);
                }
                
            }
        
            // 
            console.log("1");
        //   End
        }else if(secteur != 0){
    
            function test1(Raisone1){
                Raison_TotalAxe1S = [];
                
                for (let j = 0; j < x.length; j++) {
                    if(x[j].QSecteur.toLowerCase().trim() == secteur){
                for (let i = 0; i < uniqueReponse.length; i++) {
                    
                    
                            if(uniqueReponse[i] == x[j][Raisone1]){
                                if (!Raison_TotalAxe1S[uniqueReponse[i]]){
                                Raison_TotalAxe1S[uniqueReponse[i]] = 1;
                                } 
                                else Raison_TotalAxe1S[uniqueReponse[i]]++;
                            }
                        }
    
                    }
                    
                }
            
                
            
                return Raison_TotalAxe1S;
                }
    
            // Les Raisons
    
            for (let i = 0; i < arrayDifficultes.length; i++) {
                finalRaisonsAxe1.push(test1(arrayDifficultes[i]));
                }
                
                
                for (let i = 0; i < finalRaisonsAxe1.length; i++) {
                    if(finalRaisonsAxe1[i]["Moyennement d’accord"] == null){
                        MoyenDaccordAxe1.push(0);
                    }else{
                        MoyenDaccordAxe1.push(finalRaisonsAxe1[i]["Moyennement d’accord"]);
                    }
                    if(finalRaisonsAxe1[i]["Plutôt d’accord"] == null){
                        plutotDacordAxe1.push(0);
                    }else{
                        plutotDacordAxe1.push(finalRaisonsAxe1[i]["Plutôt d’accord"]);
                    }
                    if(finalRaisonsAxe1[i]["Tout à fait d’accord"] == null){
                        ToutAFDacordAxe1.push(0);
                    }else{
                        ToutAFDacordAxe1.push(finalRaisonsAxe1[i]["Tout à fait d’accord"]);
                    }
                    if(finalRaisonsAxe1[i]["Plutôt pas d’accord"] == null){
                        plutotPasDacordAxe1.push(0);
                    }else{
                        plutotPasDacordAxe1.push(finalRaisonsAxe1[i]["Plutôt pas d’accord"]);
                    }
                    if(finalRaisonsAxe1[i]["Pas du tout d’accord"] == null){
                        PasDeTDacordAxe1.push(0);
                    }else{
                        PasDeTDacordAxe1.push(finalRaisonsAxe1[i]["Pas du tout d’accord"]);
                    }
                    
                }
    
            // 
            console.log("2");
            //   End
        }else if(certif != 0){
        
        function test1(Raisone1){
            Raison_TotalAxe1C = [];
            
                    for (let j = 0; j < x.length; j++) {
                        
                    for (let i = 0; i < uniqueReponse.length; i++) {
                        for (let k = 0; k < 6; k++) {
                
                                if(x[j].QCertifications != null && x[j].QCertifications.split(",")[k] == certif){
                        
                                if(uniqueReponse[i] == x[j][Raisone1]){
                                    if (!Raison_TotalAxe1C[uniqueReponse[i]]){
                                    Raison_TotalAxe1C[uniqueReponse[i]] = 1;
                                    } 
                                    else Raison_TotalAxe1C[uniqueReponse[i]]++;
                                }
                            
                        }
                    }
                        
                }
            }
                    return Raison_TotalAxe1C;
                    
            
            }
        
        // Les Raisons
        
        for (let i = 0; i < arrayDifficultes.length; i++) {
            finalRaisonsAxe1.push(test1(arrayDifficultes[i]));
            
            }
            
            
            for (let i = 0; i < finalRaisonsAxe1.length; i++) {
                if(finalRaisonsAxe1[i]["Moyennement d’accord"] == null){
                    MoyenDaccordAxe1.push(0);
                }else{
                    MoyenDaccordAxe1.push(finalRaisonsAxe1[i]["Moyennement d’accord"]);
                }
                if(finalRaisonsAxe1[i]["Plutôt d’accord"] == null){
                    plutotDacordAxe1.push(0);
                }else{
                    plutotDacordAxe1.push(finalRaisonsAxe1[i]["Plutôt d’accord"]);
                }
                if(finalRaisonsAxe1[i]["Tout à fait d’accord"] == null){
                    ToutAFDacordAxe1.push(0);
                }else{
                    ToutAFDacordAxe1.push(finalRaisonsAxe1[i]["Tout à fait d’accord"]);
                }
                if(finalRaisonsAxe1[i]["Plutôt pas d’accord"] == null){
                    plutotPasDacordAxe1.push(0);
                }else{
                    plutotPasDacordAxe1.push(finalRaisonsAxe1[i]["Plutôt pas d’accord"]);
                }
                if(finalRaisonsAxe1[i]["Pas du tout d’accord"] == null){
                    PasDeTDacordAxe1.push(0);
                }else{
                    PasDeTDacordAxe1.push(finalRaisonsAxe1[i]["Pas du tout d’accord"]);
                }
                console.log(finalRaisonsAxe1[i]);
            }
        
        // 
       
        //   End
        }
        
      // ---------------Raison---------------
      Highcharts.chart('containerAXE3123', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Histo. des réponses au sujet des raisons d\'adoptation d\'un systéme de management SST'
        },
        xAxis: {
            
            categories: arrayDifficultes,
            axisLabel: {
                interval: 0,
                rotate: 30 
              }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Réponses'
            }
        },
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>:  ({point.percentage:.0f}%)<br/>',
            shared: true
        },
        plotOptions: {
            column: {
                stacking: 'percent'
            }
        },
        series: [{
            name: 'Moyennement d’accord',
            data: MoyenDaccordAxe1
        }, {
            name: 'Plutôt d’accord',
            data: plutotDacordAxe1
        }, {
            name: 'Tout à fait d’accord',
            data: ToutAFDacordAxe1
        }, {
            name: 'Plutôt pas d’accord',
            data: plutotPasDacordAxe1
        }, {
            name: 'Pas du tout d’accord',
            data: PasDeTDacordAxe1
        }]
    });
         
    }
    
    // Axe 3 ENd


function testAnalyseBYT(){

document.getElementById("showSV2").style.display = "block";
Raisone1=document.getElementById('Question2').value;
document.getElementById("titleBYT").innerHTML = "<p>% Réponses Pour la Question : <strong>"+Raisone1+"</strong> pour les Petite,Moyenne et Grande Entreprise ayant : <strong></br> OHSAS 18001 </strong></p>";
document.getElementById("titleBYTNO").innerHTML = "<p>% Réponses Pour la Question : <strong>"+Raisone1+"</strong> pour les Petite,Moyenne et Grande Entreprise : <strong></br> n’ayant pas une certification </strong></p>";
document.getElementById("titleBYTN").innerHTML = "<p>% Réponses Pour la Question : <strong>"+Raisone1+"</strong> pour les Petite,Moyenne et Grande Entreprise : <strong></br> ayant autre certification que OHSAS 18001 </strong></p>";

BYTMTotalReponse =[],BYTGTotalReponse =[],BYTPTotalReponse = [],BYTReponse=[];
Raison_TotalByCErtif = [],Raison_TotalByCErtifN = [];
Raison_TotalByCErtifG = [],Raison_TotalByCErtifGN = [];
Raison_TotalByCErtifM = [],Raison_TotalByCErtifMN = [],Raison_TotalByCErtifM1=[];
Raison_TotalNOCErtif = [],Raison_TotalNOCErtifM = [],Raison_TotalNOCErtifG = [];
BYTMNOTotalReponse=[],BYTPNOTotalReponse=[],BYTGNOTotalReponse=[];
BYTMNTotalReponse=[],BYTPNTotalReponse=[],BYTGNTotalReponse=[];

var count1=0,count2=0,count3=0;

// Petite Entreprise
for (let j = 0; j < x.length; j++) {
  
  for (let i = 0; i < uniqueReponse.length; i++) {


          for (let k = 0; k < 6; k++) {

              if(x[j].QCertifications != null && x[j].QCertifications.split(",")[k] == "OHSAS 18001"){
      
                  

                      if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés" || x[j].QNBSalarie == "Entre 100 et 200 salariés"){ 
                          if(uniqueReponse[i] == x[j][Raisone1]){
                            count1++;
                                  if (!Raison_TotalByCErtif[uniqueReponse[i]]){
                                      Raison_TotalByCErtif[uniqueReponse[i]] = 1;
                                  } 
                                  else Raison_TotalByCErtif[uniqueReponse[i]]++;
                              }
                      }

                      
                  
                      if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
                          if(uniqueReponse[i] == x[j][Raisone1]){
                            count1++;
                                  if (!Raison_TotalByCErtifG[uniqueReponse[i]]){
                                      Raison_TotalByCErtifG[uniqueReponse[i]] = 1;
                                  } 
                                  else Raison_TotalByCErtifG[uniqueReponse[i]]++;
                              }
                      }

              }

              // not OHSAS
              if(x[j].QCertifications != null){
      
                  
                  if(x[j].QCertifications.split(",")[k] != "OHSAS 18001" && x[j].QCertifications.split(",")[k] != null){
                      
                      if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés" || x[j].QNBSalarie == "Entre 100 et 200 salariés"){ 
                      
                              if(uniqueReponse[i] == x[j][Raisone1]){
                              count2++;
                              
                                      if (!Raison_TotalByCErtifN[uniqueReponse[i]]){
                                          Raison_TotalByCErtifN[uniqueReponse[i]] = 1;
                                      } 
                                      else Raison_TotalByCErtifN[uniqueReponse[i]]++;
                              }
                          }

                         

                          if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
                      
                              if(uniqueReponse[i] == x[j][Raisone1]){
                              count2++;
                              
                                      if (!Raison_TotalByCErtifGN[uniqueReponse[i]]){
                                          Raison_TotalByCErtifGN[uniqueReponse[i]] = 1;
                                      } 
                                      else Raison_TotalByCErtifGN[uniqueReponse[i]]++;
                              }
                          }
                  }

          }
          }

          // pas de certification
          if(x[j].QYN == "Non"){
            
              if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés" || x[j].QNBSalarie == "Entre 100 et 200 salariés"){ 
                  if(uniqueReponse[i] == x[j][Raisone1]){
                    count3 ++;
                          if (!Raison_TotalNOCErtif[uniqueReponse[i]]){
                              Raison_TotalNOCErtif[uniqueReponse[i]] = 1;
                          } 
                          else Raison_TotalNOCErtif[uniqueReponse[i]]++;
                      }
              }
         
              if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
                  if(uniqueReponse[i] == x[j][Raisone1]){
                    count3 ++;
                          if (!Raison_TotalNOCErtifG[uniqueReponse[i]]){
                              Raison_TotalNOCErtifG[uniqueReponse[i]] = 1;
                          } 
                          else Raison_TotalNOCErtifG[uniqueReponse[i]]++;
                      }
              }
          }
}
}
console.log(' count : ',count3);
// console.log(Raison_TotalByCErtifM1);

for (let r = 0; r < uniqueReponse.length; r++) {
BYTReponse.push(uniqueReponse[r]);
// BYTMTotalReponse.push(Raison_TotalByCErtifM[uniqueReponse[r]]);

if(Raison_TotalByCErtif[uniqueReponse[r]] == null){
    BYTPTotalReponse.push(0);
}else{
    BYTPTotalReponse.push(parseInt(((Raison_TotalByCErtif[uniqueReponse[r]]/count1)*100).toFixed()));
}
if(Raison_TotalByCErtifG[uniqueReponse[r]] == null){
    BYTGTotalReponse.push(0);
}else{
    BYTGTotalReponse.push(parseInt(((Raison_TotalByCErtifG[uniqueReponse[r]]/count1)*100).toFixed()));
}

// non ohsas
// BYTMNTotalReponse.push(Raison_TotalByCErtifMN[uniqueReponse[r]]);
if(Raison_TotalByCErtifN[uniqueReponse[r]] == null){
    BYTPNTotalReponse.push(0);
    
}
else{
    BYTPNTotalReponse.push(parseInt(((Raison_TotalByCErtifN[uniqueReponse[r]]/count2)*100).toFixed()));
    
}
if(Raison_TotalByCErtifGN[uniqueReponse[r]] == null){
   
    BYTGNTotalReponse.push(0);
}
else{
   
    BYTGNTotalReponse.push(parseInt(((Raison_TotalByCErtifGN[uniqueReponse[r]]/count2)*100).toFixed()));
}

// pas de certification
// BYTMNOTotalReponse.push(Raison_TotalNOCErtifM[uniqueReponse[r]]);
if(Raison_TotalNOCErtif[uniqueReponse[r]] == null){
    BYTPNOTotalReponse.push(0);
    
}
else{
    BYTPNOTotalReponse.push(parseInt(((Raison_TotalNOCErtif[uniqueReponse[r]]/count3)*100).toFixed()));
   
}
if(Raison_TotalNOCErtifG[uniqueReponse[r]] == null){
    
    BYTGNOTotalReponse.push(0);
}
else{
   
    BYTGNOTotalReponse.push(parseInt(((Raison_TotalNOCErtifG[uniqueReponse[r]]/count3)*100).toFixed()));
}

// console.log('la question : ',Raisone1,'par ville : ',ville,' = ',uniqueReponse[r],' : ', Raison_TotalByVille[uniqueReponse[r]]);  
}
console.log(BYTPNOTotalReponse);
// -----------------------GRAPHE-------------------

document.getElementById("showON").style.display = "block";

// ------------------------------
Highcharts.chart('TotalBYT1', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    xAxis: {
        
        categories: BYTReponse,
        axisLabel: {
            interval: 0,
            rotate: 30 
          }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Réponses'
        }
    },
    tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>:  ({point.y}%)<br/>',
        shared: true
    },
    plotOptions: {
        // column: {
        //     stacking: 'percent'
        // }
        bar: {
            dataLabels: {
              enabled: true
            }
          }
    },
    series: [{
        name: 'Petite Entreprises (PME)',
        data: BYTPTotalReponse
    }, {
        name: 'Grande Entreprise (GE)',
        data: BYTGTotalReponse
    }]
});
//   End
// ------------------------------
Highcharts.chart('TotalBYTN1', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    xAxis: {
        
        categories: BYTReponse,
        axisLabel: {
            interval: 0,
            rotate: 30 
          }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Réponses'
        }
    },
    tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>:  ({point.y}%)<br/>',
        shared: true
    },
    plotOptions: {
        // column: {
        //     stacking: 'percent'
        // }
        bar: {
            dataLabels: {
              enabled: true
            }
          }
    },
    series: [{
        name: 'Petite Entreprises (PME)',
        data: BYTPNTotalReponse
    }, {
        name: 'Grande Entreprise (GE)',
        data: BYTGNTotalReponse
    }]
});
//   End
// ------------------------------
Highcharts.chart('TotalBYTNo1', {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    xAxis: {
        
        categories: BYTReponse,
        axisLabel: {
            interval: 0,
            rotate: 30 
          }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Réponses'
        }
    },
    tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>:  ({point.y}%)<br/>',
        shared: true
    },
    plotOptions: {
        // column: {
        //     stacking: 'percent'
        // }
        bar: {
            dataLabels: {
              enabled: true
            }
          }
    },
    series: [{
        name: 'Petite Entreprises (PME)',
        data: BYTPNOTotalReponse
    }, {
        name: 'Grande Entreprise (GE)',
        data: BYTGNOTotalReponse
    }]
});
//   End

}

// ----------------- Niveau 1 ----------------- //
// par ville
function test1_CE(){

document.getElementById("showCE").style.display = "block";
Raisone1 = document.getElementById("QuestionC").value;
certif = document.getElementById("Certificas").value;
Taill = document.getElementById("Taille4").value;

CEReponse=[];
CETotalReponse=[];
Raison_TotalByCErtif = [];
for (let j = 0; j < x.length; j++) {
  
  for (let i = 0; i < uniqueReponse.length; i++) {

      // Petite Entreprise
      if(Taill == 0){

          for (let k = 0; k < 6; k++) {
     
              if(x[j].QCertifications != null){
                 
                  if(x[j].QCertifications.split(",")[k] == certif){
                      if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés" || x[j].QNBSalarie == "Entre 100 et 200 salariés"){ 
                          if(uniqueReponse[i] == x[j][Raisone1]){

                                  if (!Raison_TotalByCErtif[uniqueReponse[i]]){
                                      Raison_TotalByCErtif[uniqueReponse[i]] = 1;
                                  } 
                                  else Raison_TotalByCErtif[uniqueReponse[i]]++;
                              }
                      }
                  }
  
              }
          }
      }
      
      // Grande Entreprise
      if(Taill == 2){

          for (let k = 0; k < 6; k++) {
     
              if(x[j].QCertifications != null){
                 
                  if(x[j].QCertifications.split(",")[k] == certif){
                      if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
                          if(uniqueReponse[i] == x[j][Raisone1]){

                                  if (!Raison_TotalByCErtif[uniqueReponse[i]]){
                                      Raison_TotalByCErtif[uniqueReponse[i]] = 1;
                                  } 
                                  else Raison_TotalByCErtif[uniqueReponse[i]]++;
                              }
                      }
                  }
  
              }
          }
      }

  }
      // 
}

var finalTotalCE = [];

for (let r = 0; r < uniqueReponse.length; r++) {
    if(Raison_TotalByCErtif[uniqueReponse[r]] == null){
        CETotalReponse.push(0);
    }
    else{
        CETotalReponse.push(Raison_TotalByCErtif[uniqueReponse[r]]);
    }
  CEReponse.push(uniqueReponse[r]);
  
  // console.log('la question : ',Raisone1,'par ville : ',ville,' = ',uniqueReponse[r],' : ', Raison_TotalByVille[uniqueReponse[r]]);  
}

for(var i=0; i < CEReponse.length; i++) {
    finalTotalCE.push({
        name: CEReponse[i],
        y: CETotalReponse[i]			 
    }); 	   
} 

// alert('Operation successful');

// -----------------------GRAPHE-------------------

document.getElementById("showCE").style.display = "block";
// document.getElementById("titleVF").innerHTML = "<p>Total Réponses Pour la Question : <strong>"+Raisone1+"</strong> Par rapport a la ville :<strong>"+ville+"</strong> pour les <strong>"+$("#Taille option:selected").text();+"</strong></p>"
Highcharts.chart('TotalCE1', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Total Réponses'
    },
    xAxis: {
        
        categories: CEReponse,
        axisLabel: {
            interval: 0,
            rotate: 30 
          }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Réponses'
        }
    },
    tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>:  {point.y}<br/>',
        shared: true
    },
    plotOptions: {
        column: {
            stacking: 'value'
        }
    },
    series: [{
        name: 'Réponses',
        data: CETotalReponse
    }]
});

// pie
Highcharts.chart('TotalCE2', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: 'Total Réponses en %'
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
      point: {
        valueSuffix: '%'
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %'
        }
      }
    },
    series: [{
        name: "Réponses",
      colorByPoint: true,
      data: finalTotalCE
    }]
  });



}

// par ville
function test1_ville(){

// // document.getElementById("btnShewVF").style.display = "block";
// Raisone1 = document.getElementById("Question").value;
// ville = document.getElementById("ville").value;
// Taill = document.getElementById("Taille").value;

// FVReponse=[];
// FVTotalReponse=[];
// Raison_TotalByVille = [];
// for (let j = 0; j < x.length; j++) {
  
//   for (let i = 0; i < uniqueReponse.length; i++) {

//       // Petite Entreprise
//       if(Taill == 0){
//       if(x[j].QVille.toLowerCase().trim() == ville){
          
//           if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés"){               
//                    if(uniqueReponse[i] == x[j][Raisone1]){

//                       if (!Raison_TotalByVille[uniqueReponse[i]]){
//                           Raison_TotalByVille[uniqueReponse[i]] = 1;
//                       } 
//                       else Raison_TotalByVille[uniqueReponse[i]]++;
//                   }
//               }
//           }
//       }
//       // Moyen Entreprise
//       if(Taill == 1){
//           if(x[j].QVille.toLowerCase().trim() == ville){
              
//               if(x[j].QNBSalarie == "Entre 100 et 200 salariés"){               
//                        if(uniqueReponse[i] == x[j][Raisone1]){

//                           if (!Raison_TotalByVille[uniqueReponse[i]]){
//                               Raison_TotalByVille[uniqueReponse[i]] = 1;
//                           } 
//                           else Raison_TotalByVille[uniqueReponse[i]]++;
//                       }
//                   }
//               }
//           }
//           // Grande Entreprise
//       if(Taill == 2){
//           if(x[j].QVille.toLowerCase().trim() == ville){
              
//               if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
//                        if(uniqueReponse[i] == x[j][Raisone1]){

//                           if (!Raison_TotalByVille[uniqueReponse[i]]){
//                               Raison_TotalByVille[uniqueReponse[i]] = 1;
//                           } 
//                           else Raison_TotalByVille[uniqueReponse[i]]++;
//                       }
//                   }
//               }
//           }

//       }
//       // 
// }

for (let r = 0; r < uniqueReponse.length; r++) {
    if(Raison_TotalByVille[uniqueReponse[r]] == null){
        FVTotalReponse.push(0);
    }
    else{
        FVTotalReponse.push(Raison_TotalByVille[uniqueReponse[r]]);
    }
  FVReponse.push(uniqueReponse[r]);
  FVTotalReponse.push(Raison_TotalByVille[uniqueReponse[r]]);
  // console.log('la question : ',Raisone1,'par ville : ',ville,' = ',uniqueReponse[r],' : ', Raison_TotalByVille[uniqueReponse[r]]);  
}
// alert('Operation successful');

// -----------------------GRAPHE-------------------

document.getElementById("showFV").style.display = "block";
document.getElementById("titleVF").innerHTML = "<p>Total Réponses Pour la Question : <strong>"+Raisone1+"</strong> Par rapport a la ville :<strong>"+ville+"</strong> pour les <strong>"+$("#Taille option:selected").text();+"</strong></p>"


require(
  [
      'echarts',
      'echarts/chart/bar',
      'echarts/chart/line'
  ],

  function (ec) {

      // Initialize chart
     
      var myChartFV = ec.init(document.getElementById('TotalFV'));
  
      // ----------------Filter Question By Ville--------
     
      chartOptionsFV = {

          // Setup grid
          grid: {
              x: 40,
          
              y: 35,
              
          },

          // Add tooltip
          tooltip: {
              trigger: 'axis'
          },

          

          // Add custom colors
          color: ['#ff394f'],

          // Enable drag recalculate
          calculable: false,

          toolbox: {
              show: true,
              orient: 'vertical',
              left: 'right',
              top: 'center',
              feature: {
                  
                  saveAsImage: {show: true}
              }
          },
          // Horizontal axis
          xAxis: [{
              type: 'category',
              
              data : FVReponse
          }],

          // Vertical axis
          yAxis: [{
              type: 'value'
          }],

          // Add series
          series: [
              // 
              {
                  name: 'Precipitation',
                  type: 'bar',
                  
                  data: FVTotalReponse,
                  itemStyle: {
                      normal: {
                          label: {
                              show: true,
                              textStyle: {
                                  fontWeight: 500
                              }
                          }
                      }
                  },
                  
              }
          ]
      };

      myChartFV.setOption(chartOptionsFV);
      // Resize chart
      // ------------------------------

      $(function () {

          // Resize chart on menu width change and window resize
          $(window).on('resize', resize);
          $(".menu-toggle").on('click', resize);

          // Resize function
          function resize() {
              setTimeout(function() {

                  myChartFV.resize();
              }, 200);
          }
      });
  }
);
}

// Filter par secteur
function test1_Secteur(){

// document.getElementById("btnShewSV").style.display = "block";
Raisone1 = document.getElementById("QuestionS").value;
// Raisone1 = " [Définition des objectifs et cibles]";
secteur = document.getElementById("secteur").value;
Taill = document.getElementById("TailleS").value;

SVReponse=[];
SVTotalReponse=[];

Raison_TotalBySecteur = [];
for (let j = 0; j < x.length; j++) {
  
  for (let i = 0; i < uniqueReponse.length; i++) {

      // // Petite Entreprise
      if(Taill == 0){
      if(x[j].QSecteur.toLowerCase().trim() == secteur){
          if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés" || x[j].QNBSalarie == "Entre 100 et 200 salariés"){ 
              if(uniqueReponse[i] == x[j][Raisone1]){

                  if (!Raison_TotalBySecteur[uniqueReponse[i]]){
                      Raison_TotalBySecteur[uniqueReponse[i]] = 1;
                  } 
                      else Raison_TotalBySecteur[uniqueReponse[i]]++;
                  }
              }
          }
      }
      // 
      
      // 
       // // Grande Entreprise
       if(Taill == 2){
       if(x[j].QSecteur.toLowerCase().trim() == secteur){
          if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
              if(uniqueReponse[i] == x[j][Raisone1]){

                  if (!Raison_TotalBySecteur[uniqueReponse[i]]){
                      Raison_TotalBySecteur[uniqueReponse[i]] = 1;
                  } 
                      else Raison_TotalBySecteur[uniqueReponse[i]]++;
                  }
              }
          }
      }
      // 
  }
      
}

var finalTotalSV = [];

for (let r = 0; r < uniqueReponse.length; r++) {
    if(Raison_TotalBySecteur[uniqueReponse[r]] == null){
        SVTotalReponse.push(0);
    }
    else{
        SVTotalReponse.push(Raison_TotalBySecteur[uniqueReponse[r]]);
    }
  SVReponse.push(uniqueReponse[r]);
  
}

for(var i=0; i < SVReponse.length; i++) {
    finalTotalSV.push({
        name: SVReponse[i],
        y: SVTotalReponse[i]			 
    }); 	   
} 
// alert('Operation successful');
// ----------------------------GRAPHE------------------------------------


document.getElementById("showFS").style.display = "block";

Highcharts.chart('TotalSV1', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Total Réponses'
    },
    xAxis: {
        
        categories: SVReponse,
        axisLabel: {
            interval: 0,
            rotate: 30 
          }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Réponses'
        }
    },
    tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>:  {point.y}<br/>',
        shared: true
    },
    plotOptions: {
        column: {
            stacking: 'value'
        }
    },
    series: [{
        name: 'Réponses',
        data: SVTotalReponse
    }]
});

// pie
Highcharts.chart('TotalSV2', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: 'Total Réponses en %'
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
      point: {
        valueSuffix: '%'
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %'
        }
      }
    },
    series: [{
        name: "Réponses",
      colorByPoint: true,
      data: finalTotalSV
    }]
  });



}
// par OUI NON
function test1_ON(){

// document.getElementById("btnShewVF").style.display = "block";
Raisone1 = document.getElementById("Question3").value;
ON = document.getElementById("ON").value;
Taill = document.getElementById("Taille3").value;

ONReponse=[];
ONTotalReponse=[];
Raison_TotalByON = [];
for (let j = 0; j < x.length; j++) {
  
  for (let i = 0; i < uniqueReponse.length; i++) {

      // Petite Entreprise
      if(Taill == 0){
      if(x[j].QYN.toLowerCase().trim() == ON){
          
          if(x[j].QNBSalarie == "Entre 50 et 100 salariés" || x[j].QNBSalarie == "< 50 salariés" || x[j].QNBSalarie == "Entre 100 et 200 salariés"){               
                   if(uniqueReponse[i] == x[j][Raisone1]){

                      if (!Raison_TotalByON[uniqueReponse[i]]){
                          Raison_TotalByON[uniqueReponse[i]] = 1;
                      } 
                      else Raison_TotalByON[uniqueReponse[i]]++;
                  }
              }
          }
      }
      
          // Grande Entreprise
      if(Taill == 2){
          if(x[j].QYN.toLowerCase().trim() == ON){
              
              if(x[j].QNBSalarie == "> 500 salariés" || x[j].QNBSalarie == "Entre 400 et 500 salariés"  || x[j].QNBSalarie == "Entre 300 et 400 salariés" || x[j].QNBSalarie == "Entre 200 et 300 salariés" ){               
                       if(uniqueReponse[i] == x[j][Raisone1]){

                          if (!Raison_TotalByON[uniqueReponse[i]]){
                              Raison_TotalByON[uniqueReponse[i]] = 1;
                          } 
                          else Raison_TotalByON[uniqueReponse[i]]++;
                      }
                  }
              }
          }

      }
      // 
}
var finalTotalON = [];

for (let r = 0; r < uniqueReponse.length; r++) {
    if(Raison_TotalByON[uniqueReponse[r]] == null){
        ONTotalReponse.push(0);
    }
    else{
        ONTotalReponse.push(Raison_TotalByON[uniqueReponse[r]]);
    }
  ONReponse.push(uniqueReponse[r]);
  
  // console.log('la question : ',Raisone1,'par ville : ',ville,' = ',uniqueReponse[r],' : ', Raison_TotalByVille[uniqueReponse[r]]);  
}

for(var i=0; i < ONReponse.length; i++) {
    finalTotalON.push({
        name: ONReponse[i],
        y: ONTotalReponse[i]			 
    }); 	   
} 


// alert('Operation successful');

// -----------------------GRAPHE-------------------

document.getElementById("showON").style.display = "block";

// ---------------Raison---------------
Highcharts.chart('TotalON1', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Total Réponses'
    },
    xAxis: {
        
        categories: ONReponse,
        axisLabel: {
            interval: 0,
            rotate: 30 
          }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Réponses'
        }
    },
    tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>: {point.y} <br/>',
        shared: true
    },
    plotOptions: {
        column: {
            stacking: 'value'
        }
    },
    series: [{
        name: 'Réponses',
        data: ONTotalReponse
    }]
});

// pie
Highcharts.chart('TotalON2', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: 'Total Réponses en %'
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
      point: {
        valueSuffix: '%'
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %'
        }
      }
    },
    series: [{
        name: "Réponses",
      colorByPoint: true,
      data: finalTotalON
    }]
  });

}
// ----------------- End Niveau 1 ----------- //
// ----------------- Niveau 2 ----------- //
// Filter par secteur
function test2_SV(){

Raisone1 = document.getElementById("Question2").value;
// secteur = document.getElementById("secteur2").value;
// ville = document.getElementById("ville2").value;

SVReponse2=[];
SVTotalReponse2=[];

Raison_TotalBySecteur = [];
for (let j = 0; j < x.length; j++) {
  
  for (let i = 0; i < uniqueReponse.length; i++) {

      // 
      if(x[j].QSecteur.toLowerCase().trim() == secteur){
          if(x[j].QVille.toLowerCase().trim() == ville){

                  if(uniqueReponse[i] == x[j][Raisone1]){

                      if (!Raison_TotalBySecteur[uniqueReponse[i]]){
                          Raison_TotalBySecteur[uniqueReponse[i]] = 1;
                      } 
                      else Raison_TotalBySecteur[uniqueReponse[i]]++;
                  }
              }
          }
      }
      // 
}

for (let r = 0; r < uniqueReponse.length; r++) {
    if(Raison_TotalBySecteur[uniqueReponse[r]] == null){
        SVTotalReponse2.push(0);
    }
    else{
        SVTotalReponse2.push(Raison_TotalBySecteur[uniqueReponse[r]]);
    }
  SVReponse2.push(uniqueReponse[r]);
  
}
// alert('Operation successful');
// ----------------------------------------------------------------

document.getElementById("showSV2").style.display = "block";
document.getElementById("titleSV2").innerHTML = "<p>Total Réponses Pour la Question : <strong>"+Raisone1+"</strong> Par rapport au secteur :<strong>"+secteur+"</strong> , Ville : <strong>"+ville+"</strong></p>"


require(
  [
      'echarts',
      'echarts/chart/bar',
      'echarts/chart/line'
  ],

  function (ec) {

      // Initialize chart
     
      var myChartSV2 = ec.init(document.getElementById('TotalSV2'));
  
      // ----------------Filter Question By Ville--------
     
      chartOptionsSV2 = {

          // Setup grid
          grid: {
              x: 40,
          
              y: 35,
              
          },

          // Add tooltip
          tooltip: {
              trigger: 'axis'
          },

          

          // Add custom colors
          color: ['#ff394f'],

          // Enable drag recalculate
          calculable: false,

          toolbox: {
              show: true,
              orient: 'vertical',
              left: 'right',
              top: 'center',
              feature: {
                  
                  saveAsImage: {show: true}
              }
          },
          // Horizontal axis
          xAxis: [{
              type: 'category',
              data : SVReponse2
          }],

          // Vertical axis
          yAxis: [{
              type: 'value'
          }],

          // Add series
          series: [
              // 
              {
                  name: 'Precipitation',
                  type: 'bar',
                  
                  data: SVTotalReponse2,
                  itemStyle: {
                      normal: {
                          label: {
                              show: true,
                              textStyle: {
                                  fontWeight: 500
                              }
                          }
                      }
                  },
                  
              }
          ]
      };

      myChartSV2.setOption(chartOptionsSV2);
      // Resize chart
      // ------------------------------

      $(function () {

          // Resize chart on menu width change and window resize
          $(window).on('resize', resize);
          $(".menu-toggle").on('click', resize);

          // Resize function
          function resize() {
              setTimeout(function() {

                  // Resize chart
                  myChartSV2.resize();
              }, 200);
          }
      });
  }
);
}

// ----------------- End Niveau 1 ----------- //




function showCharts(){
    
    var finalVille = [];
    var finalSecteur = [];
    var finalSalaries =[],finalAge =[];
    
    for(var i=0; i < Ville.length; i++) {
    	finalVille.push({
            name: Ville[i],
            y: totalsVille[i]			 
        }); 	   
    }  
    for(var i=0; i < Secteur.length; i++) {
    	finalSecteur.push({
            name: Secteur[i],
            y: TotalSecteur[i]			 
        }); 	   
    } 
    for(var i=0; i < nbrsalarie.length; i++) {
    	finalSalaries.push({
            name: nbrsalarie[i],
            y: totalsalarie[i]			 
        }); 	   
    }  
    for(var i=0; i < Age.length; i++) {
    	finalAge.push({
            name: Age[i],
            y: TotalAge[i]			 
        }); 	   
    }   

document.getElementById("dashboard").style.display = "block";


Highcharts.chart('container', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: 'entreprise certifiée/labelisée ?'
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
      point: {
        valueSuffix: '%'
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %'
        }
      }
    },
    series: [{
      
      colorByPoint: true,
      data: [{
        name: 'OUI',
        y: countYes,
        sliced: true,
        selected: true
      }, {
        name: 'NON',
        y: countNon
      }]
    }]
  });
// sexe
Highcharts.chart('container1', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: 'Vous êtes ?'
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
      point: {
        valueSuffix: '%'
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %'
        }
      }
    },
    series: [{
      
      colorByPoint: true,
      data: [{
        name: 'Femmes',
        y: countF,
        sliced: true,
        selected: true
      }, {
        name: 'Hommes',
        y: countH
      }]
    }]
  });
//   Ville

Highcharts.chart('container2', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: 'Répartition géographique des PME'
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
      point: {
        valueSuffix: '%'
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %'
        }
      }
    },
    series: [{
        name: "Villes",
      colorByPoint: true,
      data: finalVille
    }]
  });

  //   Secteur

Highcharts.chart('container3', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: 'Répartition PME par Secteur d\'activité'
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
      point: {
        valueSuffix: '%'
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %'
        }
      }
    },
    series: [{
        name: "Villes",
      colorByPoint: true,
      data: finalSecteur
    }]
  });
  //   Taille

Highcharts.chart('container4', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: 'Histogramme du nombre de salariés de l\'entreprise'
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
      point: {
        valueSuffix: '%'
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %'
        }
      }
    },
    series: [{
        name: "Taille",
      colorByPoint: true,
      data: finalSalaries
    }]
  });
  //   Age

Highcharts.chart('container5', {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: 'Histogramme de l\'age de l\'ntreprise'
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
      point: {
        valueSuffix: '%'
      }
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %'
        }
      }
    },
    series: [{
        name: "Age",
      colorByPoint: true,
      data: finalAge
    }]
  });

 // ---------------Difficultés---------------
 Highcharts.chart('containerDF', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Histo. des réponses au sujet des difficultés rencontré d\'un systéme de management SST'
    },
    xAxis: {
        // categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
        categories: arrayDifficultes,
        axisLabel: {
            interval: 0,
            rotate: 30 
          }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Réponses'
        }
    },
    tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>:  ({point.percentage:.0f}%)<br/>',
        shared: true
    },
    plotOptions: {
        column: {
            stacking: 'percent'
        }
    },
    series: [{
        name: 'Moyennement d’accord',
        data: DFMoyenDaccord
    }, {
        name: 'Plutôt d’accord',
        data: DFplutotDacord
    }, {
        name: 'Tout à fait d’accord',
        data: DFToutAFDacord
    }, {
        name: 'Plutôt pas d’accord',
        data: DFplutotPasDacord
    }, {
        name: 'Pas du tout d’accord',
        data: DFPasDeTDacord
    }]
});
//   End
 // ---------------Obstacle---------------
 Highcharts.chart('containerOB', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Histo. des réponses au sujet des obstacles qui freinnent le systéme de management SST'
    },
    xAxis: {
        // categories: ['Apples', 'Oranges', 'Pears', 'Grapes', 'Bananas']
        categories: arrayObstacle,
        axisLabel: {
            interval: 0,
            rotate: 30 
          }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Réponses'
        }
    },
    tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>:  ({point.percentage:.0f}%)<br/>',
        shared: true
    },
    plotOptions: {
        column: {
            stacking: 'percent'
        }
    },
    series: [{
        name: 'Moyennement d’accord',
        data: OBMoyenDaccord
    }, {
        name: 'Plutôt d’accord',
        data: OBplutotDacord
    }, {
        name: 'Tout à fait d’accord',
        data: OBToutAFDacord
    }, {
        name: 'Plutôt pas d’accord',
        data: OBplutotPasDacord
    }, {
        name: 'Pas du tout d’accord',
        data: OBPasDeTDacord
    }]
});
//   End
 // ---------------Raison---------------
 Highcharts.chart('containerR', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Histo. des réponses au sujet des raisons d\'adoptation d\'un systéme de management SST'
    },
    xAxis: {
        
        categories: arrayRaison,
        axisLabel: {
            interval: 0,
            rotate: 30 
          }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Réponses'
        }
    },
    tooltip: {
        pointFormat: '<span style="color:{series.color}">{series.name}</span>:  ({point.percentage:.0f}%)<br/>',
        shared: true
    },
    plotOptions: {
        column: {
            stacking: 'percent'
        }
    },
    series: [{
        name: 'Moyennement d’accord',
        data: MoyenDaccord
    }, {
        name: 'Plutôt d’accord',
        data: plutotDacord
    }, {
        name: 'Tout à fait d’accord',
        data: ToutAFDacord
    }, {
        name: 'Plutôt pas d’accord',
        data: plutotPasDacord
    }, {
        name: 'Pas du tout d’accord',
        data: PasDeTDacord
    }]
});
//   End

}
