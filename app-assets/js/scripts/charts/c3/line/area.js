/*=========================================================================================
    File Name: area.js
    Description: c3 area chart
    ----------------------------------------------------------------------------------------
    Item Name: Modern Admin - Clean Bootstrap 4 Dashboard HTML Template
    Version: 1.0
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

// Area chart
// ------------------------------
$(window).on("load", function(){

    // Callback that creates and populates a data table, instantiates the area chart, passes in the data and draws it.
    var areaChart = c3.generate({
        bindto: '#area-chart',
        size: { height: 400 },
        point: {
            r: 4
        },
        color: {
            pattern: ['#E91E63']
        },

        // Create the data table.
        data: {
          xs: {
              'data1': 'x1',

          },
          columns: [
              ['x1', 10, 30, 45, 50, 70, 100],

              ['data1', 30, 200, 100, 400, 150, 250],

          ],
            types: {
                data1: 'area',
                data2: 'area-spline'
            }
        },
        grid: {
            y: {
                show: true
            }
        }
    });

    // Resize chart on sidebar width change
    $(".menu-toggle").on('click', function() {
        areaChart.resize();
    });
});
